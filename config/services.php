<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Mailgun, Postmark, AWS and more. This file provides the de facto
    | location for this type of information, allowing packages to have
    | a conventional file to locate the various service credentials.
    |
    */

    'mailgun' => [
        'domain' => env('MAILGUN_DOMAIN'),
        'secret' => env('MAILGUN_SECRET'),
        'endpoint' => env('MAILGUN_ENDPOINT', 'api.mailgun.net'),
        'scheme' => 'https',
    ],

    'postmark' => [
        'token' => env('POSTMARK_TOKEN'),
    ],

    'ses' => [
        'key' => env('AWS_ACCESS_KEY_ID'),
        'secret' => env('AWS_SECRET_ACCESS_KEY'),
        'region' => env('AWS_DEFAULT_REGION', 'us-east-1'),
    ],

'paypal'=>[
    'mode'     =>'sandbox',
    'client_id'=>'',
    'secret'   =>'',
],
'zaincash'=>[
    'msisdn'=>9647835077893, // From Merchanter
    'secret'=>'$2y$10$hBbAZo2GfSSvyqAyV2SaqOfYewgYpfR1O19gIh4SqyGWdmySZYPuS',// From Merchanter
    'merchantid'=>'5ffacf6612b5777c6d44266f',// From Merchanter
    'production_cred'=>false,
     //Test credentials or Production credentials (true=production , false=test)
    'language' => 'ar',
    'redirection_url'=>'http://127.0.0.1:8000/', // from my website
    'order_id'=>"Bill_1234567890", // from my website
    'amount'=>250,  // from my website
    'service_type'=>"WordPress Cart",
],
];
