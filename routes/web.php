<?php

use App\Http\Controllers\BookController;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\LectureController;
use App\Http\Controllers\Auth\LoginController;
use App\Http\Controllers\CheckoutController;
use App\Http\Controllers\CheckoutControllrt;
use App\Http\Controllers\DoctorController;
use App\Http\Controllers\ImpaController;
use App\Http\Controllers\NotificationController;
use App\Http\Controllers\OCRController;
use App\Http\Controllers\PlanController;
use App\Http\Controllers\ReportController;
use App\Http\Controllers\ResearchController;
use App\Http\Controllers\StoreController;
use App\Http\Controllers\student\QuestionController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\SubjectController;
use App\Http\Controllers\VideoController;
use App\Http\Controllers\ZainCashController;
use App\Models\College;
use App\Models\Subject;
use Illuminate\Support\Facades\Redis;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('products/create', [StoreController::class,'store'])->name('products.store');
///////// login in 3 types///////////// {{auth('doctor')->user()->name}}
// Route::get('/sel', function () {
//     return view('\auth\selection');
// });   NA1
// Route::get('/', [HomeController::class,'index'])->name('selection');
// Route::get('/subjects', function(){
//     return view('html\subjects')->middleware('auth');

// });
// Route::get('/test',[ReportController::class,'create']);
// Route::group(['namespace' => 'Auth'], function () {

// Route::get('/login/{type}',[LoginController::class,'loginForm'])->middleware('guest')->name('login.show');

// Route::post('/login',[LoginController::class,'login'])->name('login');

 //Route::get('/logout/{type}', [LoginController::class,'logout'])->name('logout');
//});
// Route::get('/ss', function () {
//     return view('html\subjects');
// }); NA2

// Route::get('/cc', function () {
//     return view('html\courses');
// }); NA3

// Route::get('/lan', function () {
//     return view('html\video\index');
// });
Route::get('/', function () {
    return view('html\home');
});

Route::get('/ques', function () {
   return view('html\home');
}); //after login [doctor,student]
Route::group(
    ['prefix'=>'/student',
    'as'=>'questions.',
    'namespace'=>'App\Http\Controllers\student',
    // 'middleware'=>['auth:sanctum',config('jetstream.auth_session'),'verified']
    ],
    function(){
        Route::group([
            'prefix'=>'/question',
            'middleware' => [ 'auth:student'],
            'controller'=>'QuestionController',],
            function(){
 Route::get('/create','create')->name('create');
 Route::post('/store', [QuestionController::class,'store'])->name('store');//->middleware('auth:student');
 //  Route::get('students-questions/all',  [questionController::class,'all']); //->name('questions.all'); notFinish
 //  Route::post('delete', [questionController::class,'delete'])->name('questions.delete');            notFinish
 //  Route::get('students-questions/edit/{question_id}', [questionController::class,'edit'])->name('questions.edit'); notFinish
 //  Route::post('update',[questionController::class,'update'])->name('questions.update');  notFinish

            });
        });

///////// login in 3 types/////////////

// Route::get('/pdf', function () {
//     return view('html\pdf');
// });

// Route::get('/home', function () {
//     return view('html\tbook');
// });
/////////// Start Book /////////
/////////// End Book ////////
// Route::get('/lec', function () {
//     return view('html\lecture\index');
// });
///// DownLoad///////
Route::get('lectures', [LectureController::class,'index']);
Route::get("detail/{id}",[LectureController::class,'detail']);
Route::get("/dowload",[LectureController::class,'download']);
/////End DownLoad///////

 /////// Start questions ///////
// Route::group(['prefix' => 'students'], function () {




/////////// End questions ///////////////
// Route::group(['prefix' => 'questions'], function () {
//     Route::get('create', [questionController::class,'index']);
//    Route::post('store', [questionController::class,'store']);
// });
// Route::group([
//     'prefix' => 'admin',
//     'as'    =>'admin'
// ], function () {
// Auth::routes();
// });
//////// Login Sys /////////
Auth::routes();
Route::get('/{guard}/login',[LoginController::class,'loginForm'])->middleware('guest')->name('admin.login');
Route::post('/admin/login',[LoginController::class,'loginAdmin'])->name('save.admin.login');
Route::post('/doctor/login',[LoginController::class,'loginDoctor'])->name('save.doctor.login');
Route::post('/student/login',[LoginController::class,'loginStudent'])->name('save.student.login');
//Route::post('/login/patient',[LoginController::class,'checkPatientLogin'])->name('save.lab.login');
//Route::post('/login/lab',[LoginController::class,'checkLabLogin'])->name('save.lab.login');
Route::get('logout', [LoginController::class,'logout'])->name('logout');

////////////// End Login sys //////
// Route::get('/not', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
Route::get('/ques', [QuestionController::class, 'create'])->name('home');
Route::get('create', [QuestionController::class,'create'])->name('test');
Route::get('/books', [BookController::class,'index'])->name('book');
// Route::get('/detail/{id}', [questionController::class,'show'])->name('test');
/////////// Auth Role Test//////////////

//User
// Route::middleware(['auth','user-role:user'])->group(function()
// {
//      Route::get("/home",[HomeController::class,'userHome'])->name('home');
//     Route::get("/{type}/home",[HomeController::class,'doctorHome'])->name('home');
// });

// Doctor Route
// Route::middleware(['auth','user-role:doctor'])->group(function()
// {
//     Route::get("/doctor/home",[HomeController::class,'doctorHome'])->name('home.doctor');
// });

// //Admin
// Route::middleware(['auth','user-role:admin'])->group(function()
// {
//     Route::get("/admin/home",[HomeController::class,'adminHome'])->name('home.admin');
// });
//////// profile ////////////
Route::get('/profile',[UserController::class,'profile'])->name('profile');
Route::get('/profile/edit/{user_id}',[UserController::class,'profileEdit'])->name('profile.edit');
Route::put('/profile/update/{user_id}',[UserController::class,'profileUpdate'])->name('profile.update');
Route::get('/profile/changepassword', [UserController::class,'changePasswordForm'])->name('profile.change.password');
Route::post('/profile/changepassword',[UserController::class,'changePassword'])->name('profile.changepassword');
//////// End profile ////////////



/////// Book ///////
Route::get('/book', function () {
    return view('html\book-view');
});
Route::get('/create/book', function () {
    return view('html\book\create');
});
Route::resource('/books',BookController::class,);
Route::post('/books/update/{book_id}',[BookController::class,'update'])->name('books.update');

/////// End Book //////////

//////Subjec////////
 Route::resource('/subjects',SubjectController::class,);
 //Route::get('/subjects/{name}', [SubjectController::class,'show'])->name('subjects.show');
 //Route::get('/subjects', [SubjectController::class,'index'])->name('subjects.index');
Route::post('/subjects/update/{subject_id}',[SubjectController::class,'update'])->name('subjects.update');
////// End Subject ////////

 ///////  Lecture //////

 Route::resource('/lectures',LectureController::class,);

 ////// End Lecture //////////

 ////// store///////////
Route::get('/store', function () {
    return view('\html\store');
});
Route::get('products', [StoreController::class,'index'])->name('products.index');
Route::post('products', [StoreController::class,'store'])->name('products.store');
Route::get('product/create', [StoreController::class,'create'])->name('products.create');
//////// End Store /////////

//// Report///////

Route::get('report/create',[ReportController::class,'create'])->name('reports.index');
Route::get('report/{id}',[ReportController::class,'show'])->name('reports.show');
// Route::get('reports', [ReportController::class,'create'])->name('reports.index');
Route::post('reports', [ReportController::class,'store'])->name('reports.store');

///// end Report/////////

//// Research/////
Route::get('research/create',[ResearchController::class,'create'])->name('research.index');
// Route::get('researchs', [ResearchController::class,'create'])->name('research.index');
Route::post('researchs', [ResearchController::class,'store'])->name('research.store');

///// end Research//////
Route::get('/not1', function () {
    return   $subjects1=Subject::select('name')->where('college_id','1')->get();
    ;
});
Route::get('/not2', function () {
    return view('test');
});
Route::get('/wel', function () {
    return view('welcome');
});
// Route::resource('/doctor',DoctorController::class,);
//Route::get('test',[\App\Http\Controllers\TestController::class, 'index'])->name('client.test');
//////// Search///////
Route::get('/subjects',[SubjectController::class,'search'])->name('subjects.search');
//  Route::get('/home',[HomeController::class,'search'])->name('home.search');
 Route::get('/threeSearch',[HomeController::class,'search'])->name('all.three');
 Route::get('/threeSearch2',[HomeController::class,'search2'])->name('all.three2');

 /// Search for three OBJ/////

////////// End Search /////////


/////////// test auth with view ///////
Route::get('/guard', function () {
    return view('guard');
});

///////////  paypal /////
Route::get('checkout',[CheckoutController::class ,'checkout'])->name('checkout');

Route::get('paypal/return',[CheckoutController::class ,'paypalReturn'])->name('paypal.return');
Route::get('paypal/cancel',[CheckoutController::class ,'paypalCancel'])->name('paypal.cancel');

Route::get('/ur1', function () {
    return url(route('paypal.return'));
});

Route::get('/ur2', function () {
    return route('paypal.return');
});


//////end paypal//////
Route::get('/zain',[ZainCashController::class ,'checkout'])->name('zain');
Route::get('/Zain2',[ZainCashController::class ,'zainCancel'])->name('zain.cancel');


///// Start ZainCash /////





///// End ZainCash/////
// Route::redirect('/here', '/there', 301); nice

// Route::get('/user/{id}', function (Request $request, $id) {
//     return 'User '.$id;
// });   test parameter came from user(view)
// Route::get('/search/{search}', function ($search) {
//     return $search;
// })->where('search', '.*');


////// Videos /////
Route::get('tv', [VideoController::class,'index'])->name('videos.index');
Route::post('tv', [VideoController::class,'store'])->name('videos.store');
Route::get('video/create', [VideoController::class,'create'])->name('videos.create');

//// End Videos //////

///// doctor ////
Route::get('/doctor/select',[DoctorController::class,'select']);
Route::get("/AskDoctor/{id}",[DoctorController::class,'askQ'])->name('ask.doctor');
Route::get('/ReportDoctor/{id}',[DoctorController::class,'askRe']);
Route::post('/answer/store', [DoctorController::class,'answerStore'])->name('answer.save');

// Route::get('/select/question', function () {
//     return view('html\courses');
// });
Route::get('/select/question',[DoctorController::class,'questionChoose'])->name('choose.question');
Route::get('/select/report',[DoctorController::class,'reportChoose']);
// Route::get('/select/report', function () {
//     return view('html\courses');
// });
Route::get('/select/research', function () {
    return view('html\courses');
});
Route::resource('/doctor',DoctorController::class,);
Route::get('/z', function () {
    return view('html\doctor\ask');
});
////////end doctor/////


////// start php convert////

// Route::get('/lancer',function(){
//     return view ('lancer');
// });
/////// Start OCR ///////
Route::get('/ocr',function(){
    return view ('ocr');
});
Route::get('/ocr2',function(){
    return view ('ocr2');
});
Route::post('/upload.php',function(){
    return view ('ocr2');
});
Route::post('/ocr',function(){
    return view ('ocr');
});
Route::post('/oocr',[ReportController::class,'ocr']);
Route::get('/oocr2',[ReportController::class,'ocr2']);

Route::get('/ocrep',[OCRController::class,'create']);
Route::post('/upload',[OCRController::class,'upload']);

///// end OCR convert //////
/////// Start Impa Store/////
Route::get('/imp',[ImpaController::class,'index']);
///// End Impa Store

///// Subscription////
Route::middleware("auth")->group(function () {
    Route::get('plans', [PlanController::class, 'index']);
    Route::get('plans/{plan}', [PlanController::class, 'show'])->name("plans.show");
    Route::post('subscription', [PlanController::class, 'subscription'])->name("subscription.create");
    // @if(auth()->user()->hasStripeId())

    // {{auth()->user()->stripeId()}} To Test if User subscripe

    // @endif
});

////// End Subscription /////


///// Dashboard////

Route::get('/dashoard', function () {
    return view('dashboard');
});

////End Dashboard////


///// Notification ////
Route::middleware("auth:student")->group(function () {
Route::get('notifi', [NotificationController::class, 'create']);

});
//////End Notification /////
