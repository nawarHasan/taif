<!DOCTYPE html>
<html lang="en">

<head>
      <meta charset="UTF-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <link rel="stylesheet" href="../css/videos.css">
      <link rel="preconnect" href="https://fonts.googleapis.com">
      <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
      <link href="https://fonts.googleapis.com/css2?family=Montserrat:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap"
            rel="stylesheet">
      <title>Taif</title>
</head>

<body>

      <!-- NAV BAR -->
      <header>
            <nav>
                  <div class="logo">
                        <h1><i class="fa-brands fa-slack"></i> Taif</h1>
                  </div>

                  <ul class="nav-links">
                         <li class="nav-link"><a href="/home">home</a></li>
                        <li class="nav-link"><a href="/report/create">Reports</a></li>
                        <li class="nav-link"><a href="/research/create">Research</a></li>
                        <li class="nav-link"><a href="/books">books</a></li>
                        <li class="nav-link"><a href="/subjects">subjects</a></li>
                        <li class="nav-link"><a href="/videos">videos</a></li>
                        @if (Route::has('login'))

                        @auth
                            <b><li class="nav-link">{{auth()->user()->name}}</li></b>
                        @else
                            <a href="{{ route('login') }}" ><li class="nav-link"><a href="/login" class="signup-btn">login</a></li>
                        </a>

                            @if (Route::has('register'))
                                <a href="{{ route('register') }}">  <li class="nav-link"><a href="/register" class="login-btn">signup</a></li>
                            </a>
                            @endif
                        @endauth
                    </div>
                @endif
                  </ul>
                  <div class="hamburger">
                        <span class="bar"></span>
                        <span class="bar"></span>
                        <span class="bar"></span>
                  </div>
            </nav>
      </header>
      <!-- NAV BAR END -->

      <div class="container">
            <div class="con__head">
                  <h2 class="title"> <img src="../images/proffessor.jpg"> Maher Al Harbey</h2>
                  <div class="line"></div>
            </div>






            <!-- Trending -->
            <div class="videos__section">
                  <div class="con__head__video__section">
                        <h2><i class="fa-solid fa-fire-flame-curved"></i> Trending</h2>
                  </div>

                  <div class="videos__wrapper">

                        <div class="video__card">
                              <div class="img__con">
                                    <img src="../images/maxresdefault.jpg">
                              </div>


                              <h3 class="video__title">Algebra Basics: What Are Polynomials?</h3>
                        </div>

                        <div class="video__card">
                              <div class="img__con">
                                    <img src="../images/maxresdefault.jpg">
                              </div>


                              <h3 class="video__title">Algebra Basics: What Are Polynomials?</h3>
                        </div>

                        <div class="video__card">
                              <div class="img__con">
                                    <img src="../images/maxresdefault.jpg">
                              </div>


                              <h3 class="video__title">Algebra Basics: What Are Polynomials?</h3>
                        </div>

                        <div class="video__card">
                              <div class="img__con">
                                    <img src="../images/maxresdefault.jpg">
                              </div>


                              <h3 class="video__title">Algebra Basics: What Are Polynomials?</h3>
                        </div>

                        <div class="video__card">
                              <div class="img__con">
                                    <img src="../images/maxresdefault.jpg">
                              </div>


                              <h3 class="video__title">Algebra Basics: What Are Polynomials?</h3>
                        </div>

                        <div class="video__card">
                              <div class="img__con">
                                    <img src="../images/maxresdefault.jpg">
                              </div>


                              <h3 class="video__title">Algebra Basics: What Are Polynomials?</h3>
                        </div>

                        <div class="video__card">
                              <div class="img__con">
                                    <img src="../images/maxresdefault.jpg">
                              </div>


                              <h3 class="video__title">Algebra Basics: What Are Polynomials?</h3>
                        </div>

                        <div class="video__card">
                              <div class="img__con">
                                    <img src="../images/maxresdefault.jpg">
                              </div>


                              <h3 class="video__title">Algebra Basics: What Are Polynomials?</h3>
                        </div>

                        <div class="video__card">
                              <div class="img__con">
                                    <img src="../images/maxresdefault.jpg">
                              </div>


                              <h3 class="video__title">Algebra Basics: What Are Polynomials?</h3>
                        </div>

                        <div class="video__card">
                              <div class="img__con">
                                    <img src="../images/maxresdefault.jpg">
                              </div>


                              <h3 class="video__title">Algebra Basics: What Are Polynomials?</h3>
                        </div>


                  </div>

            </div>




            <!-- Premium -->
            <div class="videos__section">
                  <div class="con__head__video__section">
                        <h2><i class="fa-solid fa-star" style="color: gold;"></i> Premium</h2>
                  </div>

                  <div class="videos__wrapper">

                        <div class="video__card" video-fees="pay">
                              <div class="lock"><i class="fa-solid fa-lock"></i></div>
                              <div class="img__con">
                                    <img src="../images/maxresdefault.jpg">
                              </div>


                              <h3 class="video__title">Algebra Basics: What Are Polynomials?</h3>
                        </div>

                        <div class="video__card" video-fees="pay">
                              <div class="lock"><i class="fa-solid fa-lock"></i></div>
                              <div class="img__con">
                                    <img src="../images/maxresdefault.jpg">
                              </div>


                              <h3 class="video__title">Algebra Basics: What Are Polynomials?</h3>
                        </div>

                        <div class="video__card" video-fees="pay">
                              <div class="lock"><i class="fa-solid fa-lock"></i></div>
                              <div class="img__con">
                                    <img src="../images/maxresdefault.jpg">
                              </div>


                              <h3 class="video__title">Algebra Basics: What Are Polynomials?</h3>
                        </div>

                        <div class="video__card" video-fees="pay">
                              <div class="lock"><i class="fa-solid fa-lock"></i></div>
                              <div class="img__con">
                                    <img src="../images/maxresdefault.jpg">
                              </div>


                              <h3 class="video__title">Algebra Basics: What Are Polynomials?</h3>
                        </div>

                        <div class="video__card" video-fees="pay">
                              <div class="lock"><i class="fa-solid fa-lock"></i></div>
                              <div class="img__con">
                                    <img src="../images/maxresdefault.jpg">
                              </div>


                              <h3 class="video__title">Algebra Basics: What Are Polynomials?</h3>
                        </div>

                        <div class="video__card" video-fees="pay">
                              <div class="lock"><i class="fa-solid fa-lock"></i></div>
                              <div class="img__con">
                                    <img src="../images/maxresdefault.jpg">
                              </div>


                              <h3 class="video__title">Algebra Basics: What Are Polynomials?</h3>
                        </div>




                  </div>

            </div>


            <div class="line"></div>
            <br>

            <!-- Textbooks -->
            <div class="textbooks__section">
                  <div class="con__head__textbooks__section">
                        <h2><i class="fa-solid fa-book" style="color: black;"></i> Maher's best Textbooks</h2>
                  </div>

                  <div class="textbooks__wrapper">

                        <div class="textbook__card">
                              <div class="img__con">
                                    <img src="../images/book.jpg">
                              </div>

                              <div class="detailes">
                                    <h3 class="textbook__title">Algebra Basics: Polynomials</h3>

                                    <a href="#">Buy</a>
                              </div>
                        </div>

                        <div class="textbook__card">
                              <div class="img__con">
                                    <img src="../images/book.jpg">
                              </div>

                              <div class="detailes">
                                    <h3 class="textbook__title">Algebra Basics: Polynomials</h3>

                                    <a href="#">Buy</a>
                              </div>
                        </div>

                        <div class="textbook__card">
                              <div class="img__con">
                                    <img src="../images/book.jpg">
                              </div>

                              <div class="detailes">
                                    <h3 class="textbook__title">Algebra Basics: Polynomials</h3>

                                    <a href="#">Buy</a>
                              </div>
                        </div>

                        <div class="textbook__card">
                              <div class="img__con">
                                    <img src="../images/book.jpg">
                              </div>

                              <div class="detailes">
                                    <h3 class="textbook__title">Algebra Basics: Polynomials</h3>

                                    <a href="#">Buy</a>
                              </div>
                        </div>

                        <div class="textbook__card">
                              <div class="img__con">
                                    <img src="../images/book.jpg">
                              </div>

                              <div class="detailes">
                                    <h3 class="textbook__title">Algebra Basics: Polynomials</h3>

                                    <a href="#">Buy</a>
                              </div>
                        </div>

                        <div class="textbook__card">
                              <div class="img__con">
                                    <img src="../images/book.jpg">
                              </div>

                              <div class="detailes">
                                    <h3 class="textbook__title">Algebra Basics: Polynomials</h3>

                                    <a href="#">Buy</a>
                              </div>
                        </div>

                        <div class="textbook__card">
                              <div class="img__con">
                                    <img src="../images/book.jpg">
                              </div>

                              <div class="detailes">
                                    <h3 class="textbook__title">Algebra Basics: Polynomials</h3>

                                    <a href="#">Buy</a>
                              </div>
                        </div>






                  </div>

            </div>

      </div>

      <!-- FOOTER -->
      <footer>
            <div class="con">
                  <h1>
                        Taif Website
                  </h1>
                  <p>© 2022 TAIF. ALL RIGHTS RESERVED.</p>

                  <div class="social__links">
                        <ul>
                              <li><a href="#"><i class="fa-brands fa-facebook"></i></a></li>
                              <li><a href="#"><i class="fa-brands fa-twitter"></i></i></a></li>
                              <li><a href="#"><i class="fa-brands fa-youtube"></i></a></li>
                              <li><a href="#"><i class="fa-brands fa-instagram"></i></a></li>
                        </ul>
                  </div>
            </div>
            <ul class="footer-links">
                  <li class="footer-link"><a href="#">home</a></li>
                  <li class="footer-link"><a href="#">lecture</a></li>
                  <li class="footer-link"><a href="#">subjects</a></li>
                  <li class="footer-link"><a href="#">videos</a></li>
                  <li class="footer-link"><a href="#">signup</a></li>
                  <li class="footer-link"><a href="#">login</a></li>

            </ul>

      </footer>
      </div>
      <link href="https://cdn.quilljs.com/1.3.6/quill.snow.css" rel="stylesheet">
      <script src="https://cdn.quilljs.com/1.3.6/quill.js"></script>
      <script src="https://kit.fontawesome.com/2c1b23ff4c.js" crossorigin="anonymous"></script>
      <script src="../js/nav.js"></script>
      <script src="../js/home.js"></script>
</body>

</html>
