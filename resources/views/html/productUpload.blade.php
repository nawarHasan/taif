<!DOCTYPE html>
<html lang="en">

<head>
      <meta charset="UTF-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <link rel="stylesheet" href="../css/productUpload.css">
      <link rel="preconnect" href="https://fonts.googleapis.com">
      <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
      <link href="https://fonts.googleapis.com/css2?family=Montserrat:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap"
            rel="stylesheet">
      <title>Taif</title>
</head>

<body>



      <!-- NAV BAR -->
      <header>
            <nav>
                  <div class="logo">
                        <h1><i class="fa-brands fa-slack"></i> Taif</h1>
                  </div>

                  <ul class="nav-links">
                         <li class="nav-link"><a href="/home">home</a></li>
                        <li class="nav-link"><a href="/write">write</a></li>
                        <li class="nav-link"><a href="/books">books</a></li>
                        <li class="nav-link"><a href="/subjects">subjects</a></li>
                        <li class="nav-link"><a href="/videos">videos</a></li>
                        <li class="nav-link"><a href="/register" class="signup-btn">signup</a></li>
                        <li class="nav-link"><a href="/login" class="login-btn">login</a></li>
                  </ul>
                  <div class="hamburger">
                        <span class="bar"></span>
                        <span class="bar"></span>
                        <span class="bar"></span>
                  </div>
            </nav>
      </header>



      <div class="container">
<br><br><br><br>
            <div class="head__con">
                  <h2><i class="fa-solid fa-upload"></i> Upload a new product!</h2>
                  <p>Insert there info then submit</p>
            </div>

            <div class="form__con">
                  <form method="POST">

                        <div class="img__uploader">
                              <label for="images" class="drop-container">
                                    <span class="drop-title">Drop files here</span>
                                    or
                                    <input type="file" id="images" accept="image/*" required>
                              </label>
                        </div>

                        <div class="input__fields">

                              <div class="input__field">
                                    <label for="product-title">Title:</label>
                                    <input type="text" id="product-title">
                              </div>

                              <div class="input__field">
                                    <label for="product-description">Description:</label>
                                    <input type="text" id="product-description">
                              </div>

                              <div class="input__field">
                                    <label for="product-author">Author:</label>
                                    <input type="text" id="product-author">
                              </div>

                              <div class="input__field">
                                    <label for="product-price">Price:</label>
                                    <input type="text" id="product-price">
                              </div>

                              <div class="input__field">
                                    <div class="date__picker">
                                          <p for="startDate">Start Date:</p>
                                          <input type="date" name="startDate" id="startDate">
                                          <p for="endDate">End Date:</p>
                                          <input type="date" name="endDate" id="endDate">
                                    </div>
                                    <button type="submit" class="upload__btn">UPLOAD</button>
                              </div>
                        </div>

                  </form>
                  <br><br><br><br><br>
            </div>

      </div>


      <!-- FOOTER -->
      <footer>
            <div class="con">
                  <h1>
                        Taif Website
                  </h1>
                  <p>© 2022 TAIF. ALL RIGHTS RESERVED.</p>

                  <div class="social__links">
                        <ul>
                              <li><a href="#"><i class="fa-brands fa-facebook"></i></a></li>
                              <li><a href="#"><i class="fa-brands fa-twitter"></i></i></a></li>
                              <li><a href="#"><i class="fa-brands fa-youtube"></i></a></li>
                              <li><a href="#"><i class="fa-brands fa-instagram"></i></a></li>
                        </ul>
                  </div>
            </div>
            <ul class="footer-links">
                  <li class="footer-link"><a href="#">home</a></li>
                  <li class="footer-link"><a href="#">write</a></li>
                  <li class="footer-link"><a href="#">subjects</a></li>
                  <li class="footer-link"><a href="#">videos</a></li>
                  <li class="footer-link"><a href="#">signup</a></li>
                  <li class="footer-link"><a href="#">login</a></li>

            </ul>

      </footer>
      </div>
      <link href="https://cdn.quilljs.com/1.3.6/quill.snow.css" rel="stylesheet">
      <script src="https://cdn.quilljs.com/1.3.6/quill.js"></script>
      <script src="https://kit.fontawesome.com/2c1b23ff4c.js" crossorigin="anonymous"></script>
      <script src="../js/nav.js"></script>
      <script src="../js/home.js"></script>
</body>

</html>
