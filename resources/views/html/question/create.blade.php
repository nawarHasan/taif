<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="/css/askExpert.css">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Montserrat:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap"
          rel="stylesheet">
    <title>Taif</title>
</head>

<body>

      <!-- NAV BAR -->
      <header>
            <nav>
                  <div class="logo">
                        <h1><i class="fa-brands fa-slack"></i> Taif</h1>
                  </div>

                  <ul class="nav-links">
                        <li class="nav-link"><a href="home">home</a></li>
                        <li class="nav-link"><a href="/questions/create">ask an expert</a></li>
                        <li class="nav-link"><a href="/products">store</a></li>
                        <li class="nav-link"><a href="/books">book</a></li>
                        <li class="nav-link"><a href="/report/create">Reports</a></li>
                        <li class="nav-link"><a href="/research/create">Researchs</a></li>
                        @if (Route::has('login'))

                            @auth
                                <b><li class="nav-link">{{auth()->user()->name}}</li></b>
                            @else
                                <a href="{{ route('login') }}" ><li class="nav-link"><a href="/login" class="signup-btn">login</a></li>
                            </a>

                                @if (Route::has('register'))
                                    <a href="{{ route('register') }}">  <li class="nav-link"><a href="/register" class="login-btn">signup</a></li>
                                </a>
                                @endif
                            @endauth
                        </div>
                    @endif
                  </ul>
                  <div class="hamburger">
                        <span class="bar"></span>
                        <span class="bar"></span>
                        <span class="bar"></span>
                  </div>
            </nav>
      </header>
      <!-- NAV BAR END -->








      <div class="flex justify-center pt-20">
        <form action="{{Route('questions.store')}}" method="POST" enctype="multipart/form-data">
            @csrf

      <!-- ASK AN EXPERT -->
      <div class="ask-expert-con">
            <p>Ask An Expert</p><br>

            <select name="#" class="subject-select">
                  <option selected disabled class="subject-option">Choose a College and Subject</option>
                  @foreach ($colleges1 as $college1 )


                  <optgroup label={{$college1->name}}>
                    @foreach ($subjects1 as $subj1)


                        <option class="subject-option">{{$subj1->name}}</option>
                        {{-- <option class="subject-option">Managment</option>
                        <option class="subject-option">Economics</option>
                        <option class="subject-option">Marketing</option>
                        <option class="subject-option">Operations Managment</option>
                        <option class="subject-option">Finance</option> --}}
                        @endforeach
                        @endforeach
                  </optgroup>
                  @foreach ($colleges2 as $college2 )


                  <optgroup label={{$college2->name}}>
                    @foreach ($subjects2 as $subj2)


                        <option class="subject-option">{{$subj2->name}}</option>
                        @endforeach
                        @endforeach
                  </optgroup>


                  @foreach ($colleges3 as $college3 )


                  <optgroup label={{$college3->name}}>
                    @foreach ($subjects3 as $subj3)


                        <option class="subject-option">{{$subj3->name}}</option>
                        @endforeach
                        @endforeach
{{--
                  <optgroup label={{$college->name}}>
                        <option class="subject-option">Algebra</option>
                        <option class="subject-option">Calcus</option>
                        <option class="subject-option">Probability</option>
                        <option class="subject-option">Advanced Math</option>
                        <option class="subject-option">Statistics</option>
                        <option class="subject-option">Trigonometry</option>
                        <option class="subject-option">Geometry</option>
                        <option class="subject-option">Advanced Math</option>
                  </optgroup>

                  <optgroup label={{$college->name}}>
                        <option class="subject-option">Anatomy and Physology</option>
                        <option class="subject-option">Earth Science</option>
                        <option class="subject-option">Biology</option>
                        <option class="subject-option">Chemistry</option>
                        <option class="subject-option">Bio-Chemistry</option>
                        <option class="subject-option">Physics</option>
                        <option class="subject-option">Nursing</option>
                        <option class="subject-option">Advanced Physics</option>
                        @endforeach
                  </optgroup> --}}

            </select>


            <input
            type="text"
            name="hent"
            placeholder="ُEnter Your hent.."
            style="padding-right: 3%;padding-bottom: 20vw; width:70vw; ">
            {{-- style="cols:100; rows:100"> --}}
      <br>
       {{-- <div id="editor">

            </div> --}}
            {{-- <input
            type="file"
            name="question"
             class="block shadow-5xl mb-10 p-2 w-80 italic placeholder-gray-400">
             <br>
<br> --}}
<br>
            {{-- <button type="submit" class="bg-green-500 block shadow-5xl mb-10 p-2 w-80 uppercase font-bold">
                  ASK The Doctor --}}

 <br>
<br>
            <input
            type="file"
            name="image"
            class="block shadow-5xl mb-10 p-2 w-80 italic placeholder-gray-400"
            placeholder="">

<br>
<br>
            <button type="submit" class="bg-green-500 block shadow-5xl mb-10 p-2 w-80 uppercase font-bold">
                Submit
            </button>
        </form>
      </div>

      <br><br><br><br>

      <br>
      <br>
      <br>
      <br>
      <br>
      <br>

      <br>
      <br>
      <br>
      <br>
      <br>
      <br>
      <!-- ASK EXPERT END -->
</div>






      <!-- FOOTER -->
      <footer>
            <div class="con">
                  <h1>
                        Taif Website
                  </h1>
                  <p>© 2022 TAIF. ALL RIGHTS RESERVED.</p>

                  <div class="social__links">
                        <ul>
                              <li><a href="#"><i class="fa-brands fa-facebook"></i></a></li>
                              <li><a href="#"><i class="fa-brands fa-twitter"></i></i></a></li>
                              <li><a href="#"><i class="fa-brands fa-youtube"></i></a></li>
                              <li><a href="#"><i class="fa-brands fa-instagram"></i></a></li>
                        </ul>
                  </div>
            </div>
            <ul class="footer-links">
                  <li class="footer-link"><a href="#">home</a></li>
                  <li class="footer-link"><a href="#">lecture</a></li>
                  <li class="footer-link"><a href="#">subjects</a></li>
                  <li class="footer-link"><a href="#">videos</a></li>
                  <li class="footer-link"><a href="#">signup</a></li>
                  <li class="footer-link"><a href="#">login</a></li>

            </ul>

      </footer>
      </div>
      <link href="https://cdn.quilljs.com/1.3.6/quill.snow.css" rel="stylesheet">
      <script src="https://cdn.quilljs.com/1.3.6/quill.js"></script>
      <script src="https://kit.fontawesome.com/2c1b23ff4c.js" crossorigin="anonymous"></script>
      <script src="../js/nav.js"></script>
      <script src="../js/home.js"></script>
</body>

</html>
