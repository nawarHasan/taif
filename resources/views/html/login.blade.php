<!DOCTYPE html>
<html lang="en">

<head>
      <link rel="preconnect" href="https://fonts.googleapis.com">
      <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
      <link href="https://fonts.googleapis.com/css2?family=Montserrat:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap"
            rel="stylesheet">
      <meta charset="UTF-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <link rel="stylesheet" href="../css/login.css">
      <title>Taif</title>
</head>

<body>



      <div class="container">

            <div class="wrapper">



                  <div class="sign__wrapper">
                        <div class="con__head">
                              <h2><i class="fa-brands fa-slack"></i> Taif</h2>
                        </div>

                        <div class="signup-detailes">


                              <h2>Welcome back!</h2>
                              <p>Let's back to study again</p>


                              <form method="post">
                                    <input type="email" placeholder="Email">
                                    <input type="password" placeholder="Password">
                                    <input type="submit" value="Login in back" class="submit-btn">
                              </form>
                              <a href="#" class="login">Signup</a>
                              <a href="#" class="login">forget password</a>
                        </div>

                  </div>


                  <div class="wrapper__img">
                        <a href="#">Signup</a>
                  </div>

            </div>

      </div>


      <link href="https://cdn.quilljs.com/1.3.6/quill.snow.css" rel="stylesheet">
      <script src="https://cdn.quilljs.com/1.3.6/quill.js"></script>
      <script src="https://kit.fontawesome.com/2c1b23ff4c.js" crossorigin="anonymous"></script>
      <script src="../js/nav.js"></script>
</body>

</html>