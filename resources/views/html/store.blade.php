<!DOCTYPE html>
<html lang="en">

<head>
      <meta charset="UTF-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <link rel="preconnect" href="https://fonts.googleapis.com">
      <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
      <link rel="stylesheet" href="../css/store.css">
      <script src="https://kit.fontawesome.com/2c1b23ff4c.js" crossorigin="anonymous"></script>
      <link href="https://fonts.googleapis.com/css2?family=Montserrat:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap"
            rel="stylesheet">
      <link rel="stylesheet" href="../css/splide.min.css">
      <title>Taif store</title>
</head>

<body>

      <!-- NAV BAR -->
      <header>
            <nav>
                  <div class="logo">
                        <h1>Taif Website</h1>
                  </div>

                  <ul class="nav-links">
                        <li class="nav-link"><a href="/home">home</a></li>
                        <li class="nav-link"><a href="/html/videos.html">lecture</a></li>
                        <li class="nav-link"><a href="/html/books.html">books</a></li>
                        <li class="nav-link"><a href="/html/subjects.html">subjects</a></li>
                        <li class="nav-link"><a href="/html/videos.html">videos</a></li>
                        <li class="nav-link"><a href="/html/signup.html" class="signup-btn">signup</a></li>
                        <li class="nav-link"><a href="/html/login.html" class="login-btn">login</a></li>
                  </ul>
                  <div class="hamburger">
                        <span class="bar"></span>
                        <span class="bar"></span>
                        <span class="bar"></span>
                  </div>
                  <div class="space">

                  </div>
            </nav>
      </header>
      <!-- NAV BAR END -->



      <div class="container">

            <section class="splide" aria-labelledby="carousel-heading">
                  <h2 id="carousel-heading"> <i class="fa-solid fa-bag-shopping"></i> View our latest offers!</h2>

                  <div class="splide__track">
                        <ul class="splide__list">
                              <li class="splide__slide"><img src="../images/cover1.png"></li>
                              <li class="splide__slide"><img src="../images/cover2.jpg"></li>
                              <li class="splide__slide"><img src="../images/books-librarey.jpg"></li>
                        </ul>
                  </div>
            </section>
      </div>

      <div class="container">
            <form method="POST">
                  <button type="submit" class="subject__upload__btn">UPLOAD A PRODUCT</button>
            </form>
            <div class="trendy__products__con">

                  <div class="con__head" style="font-size: 1rem;">
                        <h2> <i class="fa-solid fa-fire-flame-curved" style="color: rgb(255, 0, 0);"></i> Trendy
                              products on Taif website</h2>
                        <p>Get up to 40% off!</p>
                  </div>

                  <div class="outer__grid">
                        <div class="cards__container">




                              <div class="trendy__card">
                                    <div class="img__con">
                                          <img src="../images/pngwing.com.png">
                                    </div>

                                    <div class="card__detailes">
                                          <h2><a href="#">Back to school pack</a></h2>
                                          <span class="category">
                                                SCHOOL
                                          </span>
                                          <div class="stars__con"><img src="../images/5stars.png" alt=""></div>
                                          <h3 class="product__price">24.99$</h3>
                                          <button type="button" class="add__btn">Add to cart</button>
                                    </div>

                              </div>


                              <div class="trendy__card">
                                    <div class="img__con">
                                          <img src="../images/pngwing.com.png">
                                    </div>

                                    <div class="card__detailes">
                                          <h2><a href="#">Back to school pack</a></h2>
                                          <span class="category">
                                                SCHOOL
                                          </span>
                                          <div class="stars__con"><img src="../images/5stars.png" alt=""></div>
                                          <h3 class="product__price">24.99$</h3>
                                          <button type="button" class="add__btn">Add to cart</button>
                                    </div>

                              </div>


                              <div class="trendy__card">
                                    <div class="img__con">
                                          <img src="../images/pngwing.com.png">
                                    </div>

                                    <div class="card__detailes">
                                          <h2><a href="#">Back to school pack</a></h2>
                                          <span class="category">
                                                SCHOOL
                                          </span>
                                          <div class="stars__con"><img src="../images/5stars.png" alt=""></div>
                                          <h3 class="product__price">24.99$</h3>
                                          <button type="button" class="add__btn">Add to cart</button>
                                    </div>

                              </div>


                              <div class="trendy__card">
                                    <div class="img__con">
                                          <img src="../images/pngwing.com.png">
                                    </div>

                                    <div class="card__detailes">
                                          <h2><a href="#">Back to school pack</a></h2>
                                          <span class="category">
                                                SCHOOL
                                          </span>
                                          <div class="stars__con"><img src="../images/5stars.png" alt=""></div>
                                          <h3 class="product__price">24.99$</h3>
                                          <button type="button" class="add__btn">Add to cart</button>
                                    </div>

                              </div>


                              <div class="trendy__card">
                                    <div class="img__con">
                                          <img src="../images/pngwing.com.png">
                                    </div>

                                    <div class="card__detailes">
                                          <h2><a href="#">Back to school pack</a></h2>
                                          <span class="category">
                                                SCHOOL
                                          </span>
                                          <div class="stars__con"><img src="../images/5stars.png" alt=""></div>
                                          <h3 class="product__price">24.99$</h3>
                                          <button type="button" class="add__btn">Add to cart</button>
                                    </div>

                              </div>


                              <div class="trendy__card">
                                    <div class="img__con">
                                          <img src="../images/pngwing.com.png">
                                    </div>

                                    <div class="card__detailes">
                                          <h2><a href="#">Back to school pack</a></h2>
                                          <span class="category">
                                                SCHOOL
                                          </span>
                                          <div class="stars__con"></div>
                                          <h3 class="product__price">24.99$</h3>
                                          <button type="button" class="add__btn">Add to cart</button>
                                    </div>

                              </div>

                              <div class="trendy__card">
                                    <div class="img__con">
                                          <img src="../images/pngwing.com.png">
                                    </div>

                                    <div class="card__detailes">
                                          <h2><a href="#">Back to school pack</a></h2>
                                          <span class="category">
                                                SCHOOL
                                          </span>
                                          <div class="stars__con"><img src="../images/5stars.png" alt=""></div>
                                          <h3 class="product__price">24.99$</h3>
                                          <button type="button" class="add__btn">Add to cart</button>
                                    </div>

                              </div>

                              <div class="trendy__card">
                                    <div class="img__con">
                                          <img src="../images/pngwing.com.png">
                                    </div>

                                    <div class="card__detailes">
                                          <h2><a href="#">Back to school pack</a></h2>
                                          <span class="category">
                                                SCHOOL
                                          </span>
                                          <div class="stars__con"><img src="../images/5stars.png" alt=""></div>
                                          <h3 class="product__price">24.99$</h3>
                                          <button type="button" class="add__btn">Add to cart</button>
                                    </div>

                              </div>


                              <div class="trendy__card">
                                    <div class="img__con">
                                          <img src="../images/pngwing.com.png">
                                    </div>

                                    <div class="card__detailes">
                                          <h2><a href="#">Back to school pack</a></h2>
                                          <span class="category">
                                                SCHOOL
                                          </span>
                                          <div class="stars__con"><img src="../images/5stars.png" alt=""></div>
                                          <h3 class="product__price">24.99$</h3>
                                          <button type="button" class="add__btn">Add to cart</button>
                                    </div>

                              </div>

                              <div class="trendy__card">
                                    <div class="img__con">
                                          <img src="../images/pngwing.com.png">
                                    </div>

                                    <div class="card__detailes">
                                          <h2><a href="#">Back to school pack</a></h2>
                                          <span class="category">
                                                SCHOOL
                                          </span>
                                          <div class="stars__con"><img src="../images/5stars.png" alt=""></div>
                                          <h3 class="product__price">24.99$</h3>
                                          <button type="button" class="add__btn">Add to cart</button>
                                    </div>

                              </div>







                        </div>
                  </div>


            </div>







            <!-- Products section -->

            <div class="all__products__con">

                  <div class="con__head" style="font-size: 1rem;">
                        <h2> <i class="fa-solid fa-crown" style="color: rgb(255, 225, 0);"></i> Buy best quality here!
                        </h2>
                        <p>Get up to 40% off!</p>
                  </div>

                  <div class="outer__grid">
                        <div class="cards__container">




                              <div class="all__card">
                                    <div class="img__con">
                                          <img src="../images/pngwing.com.png">
                                    </div>

                                    <div class="card__detailes">
                                          <h2><a href="#">Back to school pack</a></h2>
                                          <span class="category">
                                                SCHOOL
                                          </span>
                                          <div class="stars__con"><img src="../images/5stars.png" alt=""></div>
                                          <h3 class="product__price">24.99$</h3>
                                          <button type="button" class="add__btn">Add to cart</button>
                                    </div>

                              </div>



                              <div class="all__card">
                                    <div class="img__con">
                                          <img src="../images/pngwing.com.png">
                                    </div>

                                    <div class="card__detailes">
                                          <h2><a href="#">Back to school pack</a></h2>
                                          <span class="category">
                                                SCHOOL
                                          </span>
                                          <div class="stars__con"><img src="../images/5stars.png" alt=""></div>
                                          <h3 class="product__price">24.99$</h3>
                                          <button type="button" class="add__btn">Add to cart</button>
                                    </div>

                              </div>



                              <div class="all__card">
                                    <div class="img__con">
                                          <img src="../images/pngwing.com.png">
                                    </div>

                                    <div class="card__detailes">
                                          <h2><a href="#">Back to school pack</a></h2>
                                          <span class="category">
                                                SCHOOL
                                          </span>
                                          <div class="stars__con"><img src="../images/5stars.png" alt=""></div>
                                          <h3 class="product__price">24.99$</h3>
                                          <button type="button" class="add__btn">Add to cart</button>
                                    </div>

                              </div>



                              <div class="all__card">
                                    <div class="img__con">
                                          <img src="../images/pngwing.com.png">
                                    </div>

                                    <div class="card__detailes">
                                          <h2><a href="#">Back to school pack</a></h2>
                                          <span class="category">
                                                SCHOOL
                                          </span>
                                          <div class="stars__con"><img src="../images/5stars.png" alt=""></div>
                                          <h3 class="product__price">24.99$</h3>
                                          <button type="button" class="add__btn">Add to cart</button>
                                    </div>

                              </div>



                              <div class="all__card">
                                    <div class="img__con">
                                          <img src="../images/pngwing.com.png">
                                    </div>

                                    <div class="card__detailes">
                                          <h2><a href="#">Back to school pack</a></h2>
                                          <span class="category">
                                                SCHOOL
                                          </span>
                                          <div class="stars__con"><img src="../images/5stars.png" alt=""></div>
                                          <h3 class="product__price">24.99$</h3>
                                          <button type="button" class="add__btn">Add to cart</button>
                                    </div>

                              </div>



                              <div class="all__card">
                                    <div class="img__con">
                                          <img src="../images/pngwing.com.png">
                                    </div>

                                    <div class="card__detailes">
                                          <h2><a href="#">Back to school pack</a></h2>
                                          <span class="category">
                                                SCHOOL
                                          </span>
                                          <div class="stars__con"><img src="../images/5stars.png" alt=""></div>
                                          <h3 class="product__price">24.99$</h3>
                                          <button type="button" class="add__btn">Add to cart</button>
                                    </div>

                              </div>



                              <div class="all__card">
                                    <div class="img__con">
                                          <img src="../images/pngwing.com.png">
                                    </div>

                                    <div class="card__detailes">
                                          <h2><a href="#">Back to school pack</a></h2>
                                          <span class="category">
                                                SCHOOL
                                          </span>
                                          <div class="stars__con"><img src="../images/5stars.png" alt=""></div>
                                          <h3 class="product__price">24.99$</h3>
                                          <button type="button" class="add__btn">Add to cart</button>
                                    </div>

                              </div>



                              <div class="all__card">
                                    <div class="img__con">
                                          <img src="../images/pngwing.com.png">
                                    </div>

                                    <div class="card__detailes">
                                          <h2><a href="#">Back to school pack</a></h2>
                                          <span class="category">
                                                SCHOOL
                                          </span>
                                          <div class="stars__con"><img src="../images/5stars.png" alt=""></div>
                                          <h3 class="product__price">24.99$</h3>
                                          <button type="button" class="add__btn">Add to cart</button>
                                    </div>

                              </div>



                              <div class="all__card">
                                    <div class="img__con">
                                          <img src="../images/pngwing.com.png">
                                    </div>

                                    <div class="card__detailes">
                                          <h2><a href="#">Back to school pack</a></h2>
                                          <span class="category">
                                                SCHOOL
                                          </span>
                                          <div class="stars__con"><img src="../images/5stars.png" alt=""></div>
                                          <h3 class="product__price">24.99$</h3>
                                          <button type="button" class="add__btn">Add to cart</button>
                                    </div>

                              </div>



                              <div class="all__card">
                                    <div class="img__con">
                                          <img src="../images/pngwing.com.png">
                                    </div>

                                    <div class="card__detailes">
                                          <h2><a href="#">Back to school pack</a></h2>
                                          <span class="category">
                                                SCHOOL
                                          </span>
                                          <div class="stars__con"><img src="../images/5stars.png" alt=""></div>
                                          <h3 class="product__price">24.99$</h3>
                                          <button type="button" class="add__btn">Add to cart</button>
                                    </div>

                              </div>



                              <div class="all__card">
                                    <div class="img__con">
                                          <img src="../images/pngwing.com.png">
                                    </div>

                                    <div class="card__detailes">
                                          <h2><a href="#">Back to school pack</a></h2>

                                          <div class="catogeries">
                                                <span class="category">
                                                      SCHOOL
                                                </span>
                                                <span class="category">
                                                      PAINTING
                                                </span>
                                          </div>

                                          <div class="stars__con"><img src="../images/5stars.png" alt=""></div>
                                          <h3 class="product__price">24.99$</h3>
                                          <button type="button" class="add__btn">Add to cart</button>
                                    </div>

                              </div>



                              <div class="all__card">
                                    <div class="img__con">
                                          <img src="../images/pngwing.com.png">
                                    </div>

                                    <div class="card__detailes">
                                          <h2><a href="#">Back to school pack</a></h2>
                                          <span class="category">
                                                SCHOOL
                                          </span>
                                          <div class="stars__con"><img src="../images/5stars.png" alt=""></div>
                                          <h3 class="product__price">24.99$</h3>
                                          <button type="button" class="add__btn">Add to cart</button>
                                    </div>

                              </div>



                              <div class="all__card">
                                    <div class="img__con">
                                          <img src="../images/pngwing.com.png">
                                    </div>

                                    <div class="card__detailes">
                                          <h2><a href="#">Back to school pack</a></h2>
                                          <span class="category">
                                                SCHOOL
                                          </span>
                                          <div class="stars__con"><img src="../images/5stars.png" alt=""></div>
                                          <h3 class="product__price">24.99$</h3>
                                          <button type="button" class="add__btn">Add to cart</button>
                                    </div>

                              </div>



                              <div class="all__card">
                                    <div class="img__con">
                                          <img src="../images/pngwing.com.png">
                                    </div>

                                    <div class="card__detailes">
                                          <h2><a href="#">Back to school pack</a></h2>
                                          <span class="category">
                                                SCHOOL
                                          </span>
                                          <div class="stars__con"><img src="../images/5stars.png" alt=""></div>
                                          <h3 class="product__price">24.99$</h3>
                                          <button type="button" class="add__btn">Add to cart</button>
                                    </div>

                              </div>



                              <div class="all__card">
                                    <div class="img__con">
                                          <img src="../images/pngwing.com.png">
                                    </div>

                                    <div class="card__detailes">
                                          <h2><a href="#">Back to school pack</a></h2>
                                          <span class="category">
                                                SCHOOL
                                          </span>
                                          <div class="stars__con"><img src="../images/5stars.png" alt=""></div>
                                          <h3 class="product__price">24.99$</h3>
                                          <button type="button" class="add__btn">Add to cart</button>
                                    </div>

                              </div>



                              <div class="all__card">
                                    <div class="img__con">
                                          <img src="../images/pngwing.com.png">
                                    </div>

                                    <div class="card__detailes">
                                          <h2><a href="#">Back to school pack</a></h2>
                                          <span class="category">
                                                SCHOOL
                                          </span>
                                          <div class="stars__con"><img src="../images/5stars.png" alt=""></div>
                                          <h3 class="product__price">24.99$</h3>
                                          <button type="button" class="add__btn">Add to cart</button>
                                    </div>

                              </div>



                              <div class="all__card">
                                    <div class="img__con">
                                          <img src="../images/pngwing.com.png">
                                    </div>

                                    <div class="card__detailes">
                                          <h2><a href="#">Back to school pack</a></h2>
                                          <span class="category">
                                                SCHOOL
                                          </span>
                                          <div class="stars__con"><img src="../images/5stars.png" alt=""></div>
                                          <h3 class="product__price">24.99$</h3>
                                          <button type="button" class="add__btn">Add to cart</button>
                                    </div>

                              </div>



                              <div class="all__card">
                                    <div class="img__con">
                                          <img src="../images/pngwing.com.png">
                                    </div>

                                    <div class="card__detailes">
                                          <h2><a href="#">Back to school pack</a></h2>
                                          <span class="category">
                                                SCHOOL
                                          </span>
                                          <div class="stars__con"><img src="../images/5stars.png" alt=""></div>
                                          <h3 class="product__price">24.99$</h3>
                                          <button type="button" class="add__btn">Add to cart</button>
                                    </div>

                              </div>










                        </div>
                  </div>


            </div>


            <!-- FOOTER -->
            <footer>
                  <div class="con">
                        <h1>
                              Taif Website
                        </h1>
                        <p>© 2022 TAIF. ALL RIGHTS RESERVED.</p>

                        <div class="social__links">
                              <ul>
                                    <li><a href="#"><i class="fa-brands fa-facebook"></i></a></li>
                                    <li><a href="#"><i class="fa-brands fa-twitter"></i></i></a></li>
                                    <li><a href="#"><i class="fa-brands fa-youtube"></i></a></li>
                                    <li><a href="#"><i class="fa-brands fa-instagram"></i></a></li>
                              </ul>
                        </div>
                  </div>
                  <ul class="footer-links">
                        <li class="footer-link"><a href="#">home</a></li>
                        <li class="footer-link"><a href="#">lecture</a></li>
                        <li class="footer-link"><a href="#">subjects</a></li>
                        <li class="footer-link"><a href="#">videos</a></li>
                        <li class="footer-link"><a href="#">signup</a></li>
                        <li class="footer-link"><a href="#">login</a></li>

                  </ul>

            </footer>
      </div>


      <script>
            document.addEventListener('DOMContentLoaded', function () {
                  var splide = new Splide('.splide', {
                        type: 'loop',
                        perPage: 1,
                        label: 'Taif website',
                        rewind: true
                  });
                  splide.mount();
            });
      </script>
      <script src="../js/splide.min.js"></script>
      <script src="../js/nav.js"></script>
</body>

</html>
