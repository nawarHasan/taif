<!DOCTYPE html>
<html lang="en">

<head>
      <meta charset="UTF-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <link rel="stylesheet" href="../css/sendReportResearch.css">
      <link rel="preconnect" href="https://fonts.googleapis.com">
      <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
      <link href="https://fonts.googleapis.com/css2?family=Montserrat:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap"
            rel="stylesheet">
      <title>Taif</title>
</head>

<body>

      <!-- NAV BAR -->
      <header>
            <nav>
                  <div class="logo">
                        <h1><i class="fa-brands fa-slack"></i> Taif</h1>
                  </div>

                  <ul class="nav-links">
                         <li class="nav-link"><a href="/home">home</a></li>
                        <li class="nav-link"><a href="/write">write</a></li>
                        <li class="nav-link"><a href="/books">books</a></li>
                        <li class="nav-link"><a href="/subjects">subjects</a></li>
                        <li class="nav-link"><a href="/videos">videos</a></li>
                        <li class="nav-link"><a href="/register" class="signup-btn">signup</a></li>
                        <li class="nav-link"><a href="/login" class="login-btn">login</a></li>
                  </ul>
                  <div class="hamburger">
                        <span class="bar"></span>
                        <span class="bar"></span>
                        <span class="bar"></span>
                  </div>

                  <div class="space">

                  </div>
            </nav>
      </header>
      <!-- NAV BAR END -->



      <div class="container"><br><br>
            <!-- ASK AN EXPERT -->
            <div class="ask-expert-con">
                  <p>Send a research or report</p><br>
                  <select name="#" class="subject-select">
                        <option selected disabled class="subject-option">Choose a subject</option>
                        <optgroup label="Engineering">
                              <option class="subject-option">Accounting</option>
                              <option class="subject-option">Managment</option>
                              <option class="subject-option">Economics</option>
                              <option class="subject-option">Marketing</option>
                              <option class="subject-option">Operations Managment</option>
                              <option class="subject-option">Finance</option>
                        </optgroup>

                        <optgroup label="Business">
                              <option class="subject-option">Chemical Engineering</option>
                              <option class="subject-option">Civil Engineering</option>
                              <option class="subject-option">Electrical Engineering</option>
                              <option class="subject-option">Computer Science</option>
                              <option class="subject-option">Computer Engineering</option>
                              <option class="subject-option">Mechanical Engineering</option>
                        </optgroup>

                        <optgroup label="Math">
                              <option class="subject-option">Algebra</option>
                              <option class="subject-option">Calcus</option>
                              <option class="subject-option">Probability</option>
                              <option class="subject-option">Advanced Math</option>
                              <option class="subject-option">Statistics</option>
                              <option class="subject-option">Trigonometry</option>
                              <option class="subject-option">Geometry</option>
                              <option class="subject-option">Advanced Math</option>
                        </optgroup>

                        <optgroup label="Science">
                              <option class="subject-option">Anatomy and Physology</option>
                              <option class="subject-option">Earth Science</option>
                              <option class="subject-option">Biology</option>
                              <option class="subject-option">Chemistry</option>
                              <option class="subject-option">Bio-Chemistry</option>
                              <option class="subject-option">Physics</option>
                              <option class="subject-option">Nursing</option>
                              <option class="subject-option">Advanced Physics</option>
                        </optgroup>
                  </select>


                  <div id="editor">

                  </div>

                  <div class="date__picker">
                        <p for="startDate">Start Date:</p>
                        <input type="date" name="startDate" id="startDate">
                        <p for="endDate">End Date:</p>
                        <input type="date" name="endDate" id="endDate">
                  </div>

                  <div class="wrapper">
                        <input type="radio" name="select" id="option-1" checked>
                        <input type="radio" name="select" id="option-2">
                        <label for="option-1" class="option option-1">
                              <div class="dot"></div>
                              <span>Report</span>
                        </label>
                        <label for="option-2" class="option option-2">
                              <div class="dot"></div>
                              <span>Research</span>
                        </label>
                  </div>

                  <input type="file" value="Pick an image" class="add-file"><br>
                  <input type="button" value="Send my report" class="ask-btn">

            </div>
      </div>

      <br><br><br><br>
      <br><br><br><br>
      <br><br><br><br>
      <br><br>

      <!-- FOOTER -->
      <footer>
            <div class="con">
                  <h1>
                        Taif Website
                  </h1>
                  <p>© 2022 TAIF. ALL RIGHTS RESERVED.</p>

                  <div class="social__links">
                        <ul>
                              <li><a href="#"><i class="fa-brands fa-facebook"></i></a></li>
                              <li><a href="#"><i class="fa-brands fa-twitter"></i></i></a></li>
                              <li><a href="#"><i class="fa-brands fa-youtube"></i></a></li>
                              <li><a href="#"><i class="fa-brands fa-instagram"></i></a></li>
                        </ul>
                  </div>
            </div>
            <ul class="footer-links">
                  <li class="footer-link"><a href="#">home</a></li>
                  <li class="footer-link"><a href="#">write</a></li>
                  <li class="footer-link"><a href="#">subjects</a></li>
                  <li class="footer-link"><a href="#">videos</a></li>
                  <li class="footer-link"><a href="#">signup</a></li>
                  <li class="footer-link"><a href="#">login</a></li>

            </ul>

      </footer>
      </div>
      <link href="https://cdn.quilljs.com/1.3.6/quill.snow.css" rel="stylesheet">
      <script src="https://cdn.quilljs.com/1.3.6/quill.js"></script>
      <script src="https://kit.fontawesome.com/2c1b23ff4c.js" crossorigin="anonymous"></script>
      <script src="../js/nav.js"></script>
      <script src="../js/home.js"></script>
</body>

</html>
