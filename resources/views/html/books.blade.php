<!DOCTYPE html>
<html lang="en">

<head>
      <meta charset="UTF-8">
      <link rel="preconnect" href="https://fonts.googleapis.com">
      <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
      <link href="https://fonts.googleapis.com/css2?family=Montserrat:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap"
            rel="stylesheet">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <link rel="stylesheet" href="../css/books.css">
      <title>Buy and rent books</title>
</head>

<body>

      <!-- NAV BAR -->
      <header>
            <nav>
                  <div class="logo">
                        <h1>Taif Website</h1>
                  </div>

                  <ul class="nav-links">
                         <li class="nav-link"><a href="/home">home</a></li>
                        <li class="nav-link"><a href="/lectures">lecture</a></li>
                        <li class="nav-link"><a href="/books">books</a></li>
                        <li class="nav-link"><a href="/subjects">subjects</a></li>
                        <li class="nav-link"><a href="/videos">videos</a></li>
                        {{-- <li class="nav-link"><a href="/register" class="signup-btn">signup</a></li>
                        <li class="nav-link"><a href="/login" class="login-btn">login</a></li> --}}
                  </ul>
                  <div class="hamburger">
                        <span class="bar"></span>
                        <span class="bar"></span>
                        <span class="bar"></span>
                  </div>
                  <div class="space">

                  </div>
            </nav>
      </header>
      <!-- NAV BAR END -->

      <div class="container">

            <!-- HEROSEC -->

            <div class="hero-sec">




                  <div class="detailes">
                        <h1>Build your librariy</h1>
                        <p>Buy best books in the market</p>
                        <a href="#">Get start now</a>
                  </div>




            </div>

            <!-- HEROSEC END -->

            <!-- SEARCH BAR -->

            <div class="search-bar-con">
                  <input type="text" class="search-bar" placeholder="Search for books">
                  <i class="fa-solid fa-magnifying-glass fa-2x"></i>
            </div>

            <!-- SEARCH BAR END -->

            <!-- FILTER TABS -->
            <br>
            <div class="filter-con">
                  <div class="links">
                        <a href="#">solutions</a>
                        <a href="#">textbooks</a>
                        <a href="#">videos</a>
                  </div>

                  <div class="results">results: 2,322</div>
            </div>

            <!-- FILTER TABS END -->


            <!-- BOOKS VIEW -->

            <div class="books-con">
                  <br>
                  <div class="con-head">
                        <i class="fa-solid fa-book"></i>
                        <h3>Featured books</h3>
                  </div>

                  <div class="books-view">


                        <a href="#">
                              <div class="book-card">

                                    <img src="../images/book.jpg">


                                    <div class="detailes">
                                          <h3>Quest for encluded Growth in Iraq</h3>
                                          <p>employment explanation of the life</p>
                                          <p class="author">Mark Blade</p>
                                    </div>

                              </div>
                        </a>


                        <a href="#">
                              <div class="book-card">

                                    <img src="../images/book.jpg">


                                    <div class="detailes">
                                          <h3>Quest for encluded Growth in Iraq</h3>
                                          <p>employment explanation of the life</p>
                                          <p class="author">Mark Blade</p>
                                    </div>

                              </div>
                        </a>
                        <a href="#">
                              <div class="book-card">

                                    <img src="../images/book.jpg">


                                    <div class="detailes">
                                          <h3>Quest for encluded Growth in Iraq</h3>
                                          <p>employment explanation of the life</p>
                                          <p class="author">Mark Blade</p>
                                    </div>

                              </div>
                        </a>
                        <a href="#">
                              <div class="book-card">

                                    <img src="../images/book.jpg">


                                    <div class="detailes">
                                          <h3>Quest for encluded Growth in Iraq</h3>
                                          <p>employment explanation of the life</p>
                                          <p class="author">Mark Blade</p>
                                    </div>

                              </div>
                        </a>
                        <a href="#">
                              <div class="book-card">

                                    <img src="../images/book.jpg">


                                    <div class="detailes">
                                          <h3>Quest for encluded Growth in Iraq</h3>
                                          <p>employment explanation of the life</p>
                                          <p class="author">Mark Blade</p>
                                    </div>

                              </div>
                        </a>
                        <a href="#">
                              <div class="book-card">

                                    <img src="../images/book.jpg">


                                    <div class="detailes">
                                          <h3>Quest for encluded Growth in Iraq</h3>
                                          <p>employment explanation of the life</p>
                                          <p class="author">Mark Blade</p>
                                    </div>

                              </div>
                        </a>
                        <a href="#">
                              <div class="book-card">

                                    <img src="../images/book.jpg">


                                    <div class="detailes">
                                          <h3>Quest for encluded Growth in Iraq</h3>
                                          <p>employment explanation of the life</p>
                                          <p class="author">Mark Blade</p>
                                    </div>

                              </div>
                        </a>
                        <a href="#">
                              <div class="book-card">

                                    <img src="../images/book.jpg">


                                    <div class="detailes">
                                          <h3>Quest for encluded Growth in Iraq</h3>
                                          <p>employment explanation of the life</p>
                                          <p class="author">Mark Blade</p>
                                    </div>

                              </div>
                        </a>


                  </div>

                  <br><br><br><br><br><br><br>

                  <div class="con-head">
                        <i class="fa-solid fa-hand-holding-dollar" style="color: green;"></i>
                        <h3>Free books</h3>
                  </div>

                  <div class="books-view">


                        <a href="#">
                              <div class="book-card">

                                    <img src="../images/book.jpg">


                                    <div class="detailes">
                                          <h3>Quest for encluded Growth in Iraq</h3>
                                          <p>employment explanation of the life</p>
                                          <p class="author">Mark Blade</p>
                                    </div>

                              </div>
                        </a>


                        <a href="#">
                              <div class="book-card">

                                    <img src="../images/book.jpg">


                                    <div class="detailes">
                                          <h3>Quest for encluded Growth in Iraq</h3>
                                          <p>employment explanation of the life</p>
                                          <p class="author">Mark Blade</p>
                                    </div>

                              </div>
                        </a>

                        <a href="#">
                              <div class="book-card">

                                    <img src="../images/book.jpg">


                                    <div class="detailes">
                                          <h3>Quest for encluded Growth in Iraq</h3>
                                          <p>employment explanation of the life</p>
                                          <p class="author">Mark Blade</p>
                                    </div>

                              </div>
                        </a>
                        <a href="#">
                              <div class="book-card">

                                    <img src="../images/book.jpg">


                                    <div class="detailes">
                                          <h3>Quest for encluded Growth in Iraq</h3>
                                          <p>employment explanation of the life</p>
                                          <p class="author">Mark Blade</p>
                                    </div>

                              </div>
                        </a>
                        <a href="#">
                              <div class="book-card">

                                    <img src="../images/book.jpg">


                                    <div class="detailes">
                                          <h3>Quest for encluded Growth in Iraq</h3>
                                          <p>employment explanation of the life</p>
                                          <p class="author">Mark Blade</p>
                                    </div>

                              </div>
                        </a>
                        <a href="#">
                              <div class="book-card">

                                    <img src="../images/book.jpg">


                                    <div class="detailes">
                                          <h3>Quest for encluded Growth in Iraq</h3>
                                          <p>employment explanation of the life</p>
                                          <p class="author">Mark Blade</p>
                                    </div>

                              </div>
                        </a>
                        <a href="#">
                              <div class="book-card">

                                    <img src="../images/book.jpg">


                                    <div class="detailes">
                                          <h3>Quest for encluded Growth in Iraq</h3>
                                          <p>employment explanation of the life</p>
                                          <p class="author">Mark Blade</p>
                                    </div>

                              </div>
                        </a>

                  </div>

                  <br><br><br><br>
                  <div class="line-break"></div>
                  <br><br><br>

                  <div class="con-head">
                        <i class="fa-solid fa-star" style="color: #3d15b0;"></i>
                        <h3>Premium books</h3>
                  </div>

                  <div class="books-view">


                        <a href="#">
                              <div class="book-card">

                                    <img src="../images/book.jpg">


                                    <div class="detailes">
                                          <h3>Quest for encluded Growth in Iraq</h3>
                                          <p>employment explanation of the life</p>
                                          <p class="author">Mark Blade</p>
                                    </div>

                              </div>
                        </a>
                        <a href="#">
                              <div class="book-card">

                                    <img src="../images/book.jpg">


                                    <div class="detailes">
                                          <h3>Quest for encluded Growth in Iraq</h3>
                                          <p>employment explanation of the life</p>
                                          <p class="author">Mark Blade</p>
                                    </div>

                              </div>
                        </a>
                        <a href="#">
                              <div class="book-card">

                                    <img src="../images/book.jpg">


                                    <div class="detailes">
                                          <h3>Quest for encluded Growth in Iraq</h3>
                                          <p>employment explanation of the life</p>
                                          <p class="author">Mark Blade</p>
                                    </div>

                              </div>
                        </a>
                        <a href="#">
                              <div class="book-card">

                                    <img src="../images/book.jpg">


                                    <div class="detailes">
                                          <h3>Quest for encluded Growth in Iraq</h3>
                                          <p>employment explanation of the life</p>
                                          <p class="author">Mark Blade</p>
                                    </div>

                              </div>
                        </a>

                  </div>

                  <br><br><br><br>



            </div>

            <!-- BOOKS VIEW END -->

      </div>


      <footer>
            <div class="con">
                  <h1>
                        Taif Website
                  </h1>
                  <p>© 2022 TAIF. ALL RIGHTS RESERVED.</p>

                  <div class="social__links">
                        <ul>
                              <li><a href="#"><i class="fa-brands fa-facebook"></i></a></li>
                              <li><a href="#"><i class="fa-brands fa-twitter"></i></i></a></li>
                              <li><a href="#"><i class="fa-brands fa-youtube"></i></a></li>
                              <li><a href="#"><i class="fa-brands fa-instagram"></i></a></li>
                        </ul>
                  </div>
            </div>
            <ul class="footer-links">
                  <li class="footer-link"><a href="#">home</a></li>
                  <li class="footer-link"><a href="#">lecture</a></li>
                  <li class="footer-link"><a href="#">subjects</a></li>
                  <li class="footer-link"><a href="#">videos</a></li>
                  <li class="footer-link"><a href="#">signup</a></li>
                  <li class="footer-link"><a href="#">login</a></li>

            </ul>

      </footer>

      <script src="https://kit.fontawesome.com/2c1b23ff4c.js" crossorigin="anonymous"></script>
      <script src="../js/nav.js"></script>
      <script src="../js/books.js"></script>
</body>

</html>
