<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
<div class="row">
    <div class="col-md-12 mb-30">
        <div class="card card-statistics h-100">
            <div class="card-body">

                <form action="{{route('subjects.search')}}" method="get">
                    <label for="name">search</label>
                    <input type="text" name="name" placeholder="enter subjectName">
                    <button type="submit" class="btn btn-primary">search</button>

                     </form>
                <table class="table" class="display">
                    <thead>
                        <tr>
                            <th>Subject</th>
                            <th >College</th>
                        </tr>
                    </thead>
                    <tbody>

                        @foreach ($subjects as $subject)
                        <tr>
                            <td>
                        <a href="{{Route('subjects.show',$subject->name)}}" style="text-decoration-line: none"> {{ $subject->name }}
                        </a>
                        </td>
                            <td>{{ $subject->colleges->name }}</td>
                            @endforeach
                        </tr>


                    </tbody>
                </table>
            </div>
        </div>
