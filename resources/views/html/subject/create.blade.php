<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
<div class="page-title">
    <div class="row">
        <div class="col-sm-6">
            <h4 class="mb-0" style="color: #556AFF;"> <b>Create Subject</b> </h4>
        </div>

    </div>
</div>

<div class="row">
    <div class="col-md-12 mb-30">
        <div class="card card-statistics h-100">
            <div class="card-body">



                <form method="post" enctype="multipart/form-data" action="{{Route('subjects.store')}}" autocomplete="off">

                    @csrf

                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="name">name<span class="text-danger">*</span></label>
                                <input  type="text" name="name"  class="form-control" placeholder="Subject_name">
                                @error('name')
                                <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>

                        {{-- <div class="col-md-4">
                            <div class="form-group">
                                <label for="photo"> file<span class="text-danger"></span></label>
                                 <input  class="form-control" name="photo" type="file" accept="image/*">
                                 @error('file')
                                 <div class="alert alert-danger">{{ $message }}</div>
                                 @enderror
                 </div>
                 {{-- </div>                   ///// if i want file column ////// --}}

                    <div class="col-md-4">
                        <div class="form-group">
                 <label for="college_id">College<span class="text-danger">*</span></label>
                 <select name="college_id" class="form-control">
                     <option selected disabled>Choose College</option>
                     @foreach($colleges as $college)
                     <option  >{{ $college->id }}</option>
                     @endforeach
                 </select>
                </div>
            </div>
                 @error('college_id')
                 <div class="alert alert-danger">{{ $message }}</div>
                 @enderror


<div style="padding-left:660px ">

                   {{-- <a class="btn btn-outline-primary btn-md  btn-lg" type="submit" href="/Event/event/create" >save and create another</a> --}}
<br>
                  <button type="submit" style="margin: 10px;" class="btn btn-primary btn-md  btn-lg" >Save</button>
                </div>
                </form>


            </div>
        </div>
    </div>
</div>
