<!DOCTYPE html>
<html lang="en">

<head>
      <meta charset="UTF-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <link rel="stylesheet" href="../css/askExpert.css">
      <link rel="preconnect" href="https://fonts.googleapis.com">
      <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
      <link href="https://fonts.googleapis.com/css2?family=Montserrat:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap"
            rel="stylesheet">
      <title>Taif</title>
</head>

<body>

      <!-- NAV BAR -->

      <!-- NAV BAR END -->








<div class="container">
    @foreach ($questions as $question)
      <!-- ASK AN EXPERT -->
      <div class="ask-expert-con">
        <embed style="height: 250px; width:255px" src="../images/Quesions/{{$question->question}}">

            @endforeach
            <div id="editor">

            </div>


            <input type="button" value="Answer" class="ask-btn">

      </div>

      <br><br><br><br>
      <!-- ASK EXPERT END -->
</div>






      <!-- FOOTER -->
</div>
      <link href="https://cdn.quilljs.com/1.3.6/quill.snow.css" rel="stylesheet">
      <script src="https://cdn.quilljs.com/1.3.6/quill.js"></script>
      <script src="https://kit.fontawesome.com/2c1b23ff4c.js" crossorigin="anonymous"></script>
      <script src="../js/nav.js"></script>
      <script src="../js/home.js"></script>
</body>

</html>
