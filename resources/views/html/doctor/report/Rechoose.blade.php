<!DOCTYPE html>
<html lang="en">

<head>
      <meta charset="UTF-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <link rel="preconnect" href="https://fonts.googleapis.com">
      <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
      <link rel="stylesheet" href="../css/courses.css">
      <script src="https://kit.fontawesome.com/2c1b23ff4c.js" crossorigin="anonymous"></script>
      <link href="https://fonts.googleapis.com/css2?family=Montserrat:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap"
            rel="stylesheet">
      <link rel="stylesheet" href="../css/splide.min.css">
      <title>Taif store</title>
</head>

<body>

      <!-- NAV BAR -->
      <header>
            <nav>
                  <div class="logo">
                        <h1>Taif Website</h1>
                  </div>

                  <ul class="nav-links">
                         <li class="nav-link"><a href="/home">home</a></li>
                        <li class="nav-link"><a href="/lectures">lecture</a></li>
                        <li class="nav-link"><a href="/books">books</a></li>
                        <li class="nav-link"><a href="/subjects">subjects</a></li>
                        {{-- <li class="nav-link"><a href="/videos">videos</a></li> --}}
                        <li class="nav-link"><a href="/register" class="signup-btn">signup</a></li>
                        <li class="nav-link"><a href="/login" class="login-btn">login</a></li>
                  </ul>
                  <div class="hamburger">
                        <span class="bar"></span>
                        <span class="bar"></span>
                        <span class="bar"></span>
                  </div>
                  <div class="space">

                  </div>
            </nav>
      </header>
      <!-- NAV BAR END -->


      <section class="splide" aria-labelledby="carousel-heading">
            <div class="splide__track">
                  <ul class="splide__list">
                        <li class="splide__slide">
                              <div class="users__con">
                                    <h2><i class="fa-solid fa-comment"></i> Reports </h2><br>
                                    <ul>
                                         @foreach($questions as $question)
                                          <li class="user__item">
                                                <div class="user__img__con">
                                                      <img src="../images/proffessor.jpg">
                                                </div>
                                                <a href="/ReportDoctor/{{$question->id}}">
                                                <h3 class="user__name">report number {{$question->id}}</h3>
                                                </a>
                                                <div class="status">
                                                      <i class="fa-sharp fa-solid fa-circle-dot red_status"></i>
                                                </div>
                                          </li>
                                          @endforeach
                                    </ul>
                              </div>
                        </li>


                        <li class="splide__slide">
                              <h2 class="grid__title"><i class="fa-solid fa-circle-check"
                                          style="color: rgb(0, 157, 0);"></i> Active Courses</h2>
                              <div class="active__courses__con">




                                    <div class="all__card">
                                          <div class="img__con">
                                                <img src="../images/pngwing.com.png">
                                          </div>

                                          <div class="card__detailes">
                                                <h2><a href="#">Back to school pack</a></h2>
                                                <span class="category">
                                                      SCHOOL
                                                </span>
                                                <div class="stars__con"><img src="../images/5stars.png" alt=""></div>
                                          </div>

                                    </div>

                                    <div class="all__card">
                                          <div class="img__con">
                                                <img src="../images/pngwing.com.png">
                                          </div>

                                          <div class="card__detailes">
                                                <h2><a href="#">Back to school pack</a></h2>
                                                <span class="category">
                                                      SCHOOL
                                                </span>
                                                <div class="stars__con"><img src="../images/5stars.png" alt=""></div>
                                          </div>

                                    </div>

                                    <div class="all__card">
                                          <div class="img__con">
                                                <img src="../images/pngwing.com.png">
                                          </div>

                                          <div class="card__detailes">
                                                <h2><a href="#">Back to school pack</a></h2>
                                                <span class="category">
                                                      SCHOOL
                                                </span>
                                                <div class="stars__con"><img src="../images/5stars.png" alt=""></div>
                                          </div>

                                    </div>

                                    <div class="all__card">
                                          <div class="img__con">
                                                <img src="../images/pngwing.com.png">
                                          </div>

                                          <div class="card__detailes">
                                                <h2><a href="#">Back to school pack</a></h2>
                                                <span class="category">
                                                      SCHOOL
                                                </span>
                                                <div class="stars__con"><img src="../images/5stars.png" alt=""></div>
                                          </div>

                                    </div>

                                    <div class="all__card">
                                          <div class="img__con">
                                                <img src="../images/pngwing.com.png">
                                          </div>

                                          <div class="card__detailes">
                                                <h2><a href="#">Back to school pack</a></h2>
                                                <span class="category">
                                                      SCHOOL
                                                </span>
                                                <div class="stars__con"><img src="../images/5stars.png" alt=""></div>
                                          </div>

                                    </div>






                              </div>
                        </li>


                        <li class="splide__slide">
                              <h2 class="grid__title"><i class="fa-solid fa-square-plus"
                                          style="color: rgb(50, 50, 223);"></i> Create a course</h2>
                              <div class="create__course__con">

                                    <form method="POST">

                                          <div class="input__fields">

                                                <div class="input__field">
                                                      <label for="course__title">
                                                            Title
                                                      </label>
                                                      <input type="text" id="course__title">
                                                </div>

                                                <div class="input__field">
                                                      <label for="course__description">
                                                            Course description
                                                      </label>
                                                      <input type="text" id="course__description">
                                                </div>

                                                <div class="input__field">
                                                      <label for="course__specification">
                                                            Specification
                                                      </label>
                                                      <input type="text" id="course__specification">
                                                </div>

                                                <div class="input__field">
                                                      <label for="course__period">
                                                            Period
                                                      </label>
                                                      <input type="text" id="course__period">
                                                </div>

                                                <div class="input__field">
                                                      <label for="course__price">
                                                            Price
                                                      </label>
                                                      <input type="text" id="course__price">
                                                </div>


                                                <div class="img__uploader">
                                                      <label for="images" class="drop-container">
                                                            <span class="drop-title">Drop files here</span>
                                                            or
                                                            <input type="file" id="images" accept="image/*" required>
                                                      </label>
                                                </div>


                                                <input type="submit" value="Upload" class="upload__btn">
                                          </div>

                                    </form>

                              </div>
                        </li>
                  </ul>
            </div>
      </section>


      </div>


      <script>
            document.addEventListener('DOMContentLoaded', function () {
                  var splide = new Splide('.splide', {
                        type: 'loop',
                        perPage: 1,
                        rewind: true,
                        arrows: false,
                        pagination: false
                  });
                  splide.mount();
            });
      </script>

      <script src="../js/splide.min.js"></script>

      <script src="../js/nav.js"></script>
</body>

</html>
