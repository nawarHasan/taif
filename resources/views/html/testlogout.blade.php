@extends('layouts.app')

@section('content')
<!DOCTYPE html>
<html lang="en">

<head>
      <meta charset="UTF-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <link rel="stylesheet" href="/css/home.css">
      <link rel="preconnect" href="https://fonts.googleapis.com">
      <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
      <link href="https://fonts.googleapis.com/css2?family=Montserrat:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap"
            rel="stylesheet">

        <link href="{{ asset('css/app.css') }}" rel="stylesheet">
        <script src="{{ asset('js/app.js') }}" defer></script>
        <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.bunny.net/css?family=Nunito" rel="stylesheet">

    <!-- Scripts -->
    @vite(['resources/sass/app.scss', 'resources/js/app.js'])
      <title>Taif</title>
</head>

<body class="container">

      <!-- NAV BAR -->
      {{-- <header> --}}
        <div id="app">
            <nav class="navbar navbar-expand-md navbar-light bg-white shadow-sm">
                <div class="container">
                    <h3 class="mx-auto text-center">

                    <h5 class="mx-auto text-center d-flex">
                    @auth
                        <a class="nav-link text-center" href="{{ route('test') }}">
                            {{ auth()->user()->name }}
                        </a>
                        <a class="nav-link text-center-lg" onclick="event.preventDefault();document.getElementById('logout-form').submit();" href="{{ route('logout') }}">
                            Logout
                        </a>
                    @endauth
                    <form class="d-none" action="{{ route('logout') }}" id="logout-form" method="post">
                        @csrf

                    </form>
                </h5>
            </div>
                  <ul class="nav-links">
                        <li class="nav-link"><a href="/home">hommme</a></li>
                        <li class="nav-link"><a href="/write">write</a></li>
                        <li class="nav-link"><a href="/books">books</a></li>
                        <li class="nav-link"><a href="/subjects">subjects</a></li>
                        <li class="nav-link"><a href="/videos">videos</a></li>
                        <li class="nav-link"><a href="/register" class="signup-btn">signup</a></li>
                        <li class="nav-link"><a href="/login" class="login-btn">login</a></li>
                  </ul>
                  <div class="hamburger">
                        <span class="bar"></span>
                        <span class="bar"></span>
                        <span class="bar"></span>
                  </div>
            </nav>
      </header>
      <!-- NAV BAR END -->

      <!-- HEROSEC -->
      <div class="hero-sec">
            <h2>
                  Best way to start <i>Studing and learning</i>
            </h2>

            <div class="search-bar-con">
                  <h4>Find a solution, search and discover.</h4>
                  <div class="input-con">
                        <input type="text" class="search-bar" placeholder="Search for text, books or doctors">
                        <i class="fa-solid fa-magnifying-glass"></i>
                  </div>
            </div>
      </div> <br><br><br>
      <!-- HEROSEC END -->

      <br><br>

      <div class="container">


            <!-- FEATURES -->

            <div class="features-con">

                  <div class="card">
                        <div class="img__con">
                              <img src="../images/homework.png">
                        </div>
                        <h4>help you in your homework</h4>
                  </div>

                  <div class="card">
                        <div class="img__con">
                              <img src="../images/exam.png">
                        </div>
                        <h4>prepare you for exams</h4>
                  </div>

                  <div class="card">
                        <div class="img__con">
                              <img src="../images/star.png">
                        </div>
                        <h4>make you better student</h4>
                  </div>

                  <div class="card">
                        <div class="img__con">
                              <img src="../images/flexibility.png">
                        </div>
                        <h4>more flexibiltey with understanding</h4>
                  </div>

            </div>
            <br><br>
            <!-- FEATURES END -->


            <!-- SECTIONS-PANEL -->
            <div class="sections">
                  <div class="books-library">
                        <img src="../images/books-librarey.jpg">
                        <div class="detailes">
                              <h2>Find your books from a huge library</h2>
                              <p>Pay less, Save more!</p><br>
                              <a href="#">Browse books</a>
                        </div>
                  </div>

                  <div class="problems-fix">
                        <img src="../images/noProblemsafternow.jpg">
                        <div class="detailes">
                              <h2>No problems from now!</h2>
                              <p>You have a problem, OK, Taif Website will fix it for you.</p><br>
                              <a href="#">Ask an expert</a>
                        </div>
                  </div>

                  <div class="future-wait">
                        <img src="../images/graduation-7287004.jpg">
                        <div class="detailes">
                              <h2>The future is waiting for you</h2>
                              <p>Start building your mind from scratch</p><br>
                              <a href="#">Let's get started</a>
                        </div>
                  </div>
            </div>

            <br><br><br>
            <br><br><br>
            <!-- SECTIONS-PANEL END -->

            <!-- FOOTER -->
            <footer>
                  <div class="con">
                        <h1>
                              Taif Website
                        </h1>
                        <p>© 2022 TAIF. ALL RIGHTS RESERVED.</p>

                        <div class="social__links">
                              <ul>
                                    <li><a href="#"><i class="fa-brands fa-facebook"></i></a></li>
                                    <li><a href="#"><i class="fa-brands fa-twitter"></i></i></a></li>
                                    <li><a href="#"><i class="fa-brands fa-youtube"></i></a></li>
                                    <li><a href="#"><i class="fa-brands fa-instagram"></i></a></li>
                              </ul>
                        </div>
                  </div>
                  <ul class="footer-links">
                        <li class="footer-link"><a href="#">home</a></li>
                        <li class="footer-link"><a href="#">write</a></li>
                        <li class="footer-link"><a href="#">subjects</a></li>
                        <li class="footer-link"><a href="#">videos</a></li>
                        <li class="footer-link"><a href="#">signup</a></li>
                        <li class="footer-link"><a href="#">login</a></li>

                  </ul>

            </footer>
      </div>
      <link href="https://cdn.quilljs.com/1.3.6/quill.snow.css" rel="stylesheet">
      <script src="https://cdn.quilljs.com/1.3.6/quill.js"></script>
      <script src="https://kit.fontawesome.com/2c1b23ff4c.js" crossorigin="anonymous"></script>
      <script src="../js/nav.js"></script>
      <script src="../js/home.js"></script>
</body>

</html>
