<!DOCTYPE html>
<html lang="en">

<head>
      <meta charset="UTF-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <link rel="stylesheet" href="../css/subjects.css">
      <link rel="preconnect" href="https://fonts.googleapis.com">
      <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
      <link href="https://fonts.googleapis.com/css2?family=Montserrat:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap"
            rel="stylesheet">
      <title>Taif</title>
</head>

<body>

      <!-- NAV BAR -->
      <header>
            <nav>
                  <div class="logo">
                        <h1><i class="fa-brands fa-slack"></i> Taif</h1>
                  </div>

                  <ul class="nav-links">
                         <li class="nav-link"><a href="/home">home</a></li>
                        <li class="nav-link"><a href="/lectures">lecture</a></li>
                        <li class="nav-link"><a href="/books">books</a></li>
                        <li class="nav-link"><a href="/subjects">subjects</a></li>
                        <li class="nav-link"><a href="/videos">videos</a></li>
                        {{-- <li class="nav-link"><a href="/register" class="signup-btn">signup</a></li>
                        <li class="nav-link"><a href="/login" class="login-btn">login</a></li> --}}
                  </ul>
                  <div class="hamburger">
                        <span class="bar"></span>
                        <span class="bar"></span>
                        <span class="bar"></span>
                  </div>
                  <div class="space">

                  </div>
            </nav>
      </header>



      <div class="container">

            <div class="con__head">
                  <h2>Choose a subject to start</h2>
                  <p>each subject has there doctors</p>
            </div>

            <div class="subjects__wrapper">


                  <div class="subject__card" style="background-image: url('../images/math.jpeg');">
                        <h2><a href="#">Math</a></h2>
                  </div>

                  <div class="subject__card" style="background-image: url('../images/arabic.jpg');">
                        <h2><a href="#">Arabic</a></h2>
                  </div>

                  <div class="subject__card" style="background-image: url('../images/science.jpg');">
                        <h2><a href="#">Science</a></h2>
                  </div>

                  <div class="subject__card" style="background-image: url('../images/english.jpg');">
                        <h2><a href="#">English</a></h2>
                  </div>

                  <div class="subject__card" style="background-image: url('../images/engineering.jpg');">
                        <h2><a href="#">Engineering</a></h2>
                  </div>

                  <div class="subject__card" style="background-image: url('../images/datascience.jpeg');">
                        <h2><a href="#">Data Science</a></h2>
                  </div>



            </div>


            <form method="POST">
                  <button type="submit" class="subject__upload__btn">UPLOAD A SUBJECT</button>
            </form>

      </div>



      <footer>
            <div class="con">
                  <h1>
                        Taif Website
                  </h1>
                  <p>© 2022 TAIF. ALL RIGHTS RESERVED.</p>

                  <div class="social__links">
                        <ul>
                              <li><a href="#"><i class="fa-brands fa-facebook"></i></a></li>
                              <li><a href="#"><i class="fa-brands fa-twitter"></i></i></a></li>
                              <li><a href="#"><i class="fa-brands fa-youtube"></i></a></li>
                              <li><a href="#"><i class="fa-brands fa-instagram"></i></a></li>
                        </ul>
                  </div>
            </div>
            <ul class="footer-links">
                  <li class="footer-link"><a href="#">home</a></li>
                  <li class="footer-link"><a href="#">lecture</a></li>
                  <li class="footer-link"><a href="#">subjects</a></li>
                  <li class="footer-link"><a href="#">videos</a></li>
                  <li class="footer-link"><a href="#">signup</a></li>
                  <li class="footer-link"><a href="#">login</a></li>

            </ul>

      </footer>
      </div>
      <link href="https://cdn.quilljs.com/1.3.6/quill.snow.css" rel="stylesheet">
      <script src="https://cdn.quilljs.com/1.3.6/quill.js"></script>
      <script src="https://kit.fontawesome.com/2c1b23ff4c.js" crossorigin="anonymous"></script>
      <script src="../js/nav.js"></script>
      <script src="../js/doctors.js"></script>
</body>

</html>
