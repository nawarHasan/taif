<!DOCTYPE html>
<html lang="en">

<head>
      <meta charset="UTF-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <link rel="stylesheet" href="../css/book-view.css">
      <link rel="preconnect" href="https://fonts.googleapis.com">
      <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
      <link href="https://fonts.googleapis.com/css2?family=Montserrat&display=swap" rel="stylesheet">
      <title>Buy and rent books</title>
</head>

<body>



      <!-- NAV BAR -->
      <header>
            <nav>
                  <div class="logo">
                        <h1><i class="fa-brands fa-slack"></i> Taif</h1>
                  </div>

                  <ul class="nav-links">
                         <li class="nav-link"><a href="/home">home</a></li>
                        <li class="nav-link"><a href="/lectures">lecture</a></li>
                        <li class="nav-link"><a href="/books">books</a></li>
                        <li class="nav-link"><a href="/subjects">subjects</a></li>
                        <li class="nav-link"><a href="/videos">videos</a></li>
                        {{-- <li class="nav-link"><a href="/register" class="signup-btn">signup</a></li>
                        <li class="nav-link"><a href="/login" class="login-btn">login</a></li> --}}
                  </ul>
                  <div class="hamburger">
                        <span class="bar"></span>
                        <span class="bar"></span>
                        <span class="bar"></span>
                  </div>
                  <div class="space">

                  </div>
            </nav>
      </header>
      <!-- NAV BAR END -->



      <div class="container">
            <div class="book__card">

                  <div class="card__img">
                        <img src="../images/book.jpg">
                  </div>

                  <div class="detailes">
                        <h1>Quest for encluded Growth in Iraq</h1>
                        <p>employment explanation of the life</p>
                        <p>Author: Muhammed Salem</p>
                        <p>Price: $4</p>
                        <p class="ssss">Release date: 2014/3/22</p>
                        <a href="#">Buy the book</a>
                  </div>
                  <br>
                  <div class="payment__methods">
                        <img src="../images/paymentMethods.png" class="payment__method__img">
                  </div>
            </div>
      </div>

      <br><br>


      <script src="https://kit.fontawesome.com/2c1b23ff4c.js" crossorigin="anonymous"></script>
      <script src="../js/nav.js"></script>
      <script src="../js/books.js"></script>
</body>

</html>
