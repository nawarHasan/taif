<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
<div class="page-title">
    <div class="row">
        <div class="col-sm-6">
            <h4 class="mb-0" style="color: #556AFF;"> <b>Create Video</b> </h4>
        </div>

    </div>
</div>

<div class="row">
    <div class="col-md-12 mb-30">
        <div class="card card-statistics h-100">
            <div class="card-body">



                <form method="post" enctype="multipart/form-data" action="{{Route('videos.store')}}" autocomplete="off">

                    @csrf

                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="name">author<span class="text-danger">*</span></label>
                                <input  type="text" name="author"  class="form-control" placeholder="author">
                                @error('event_name')
                                <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>


                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="price">type<span class="text-danger">*</span></label>
                                <input  class="form-control" name="type" type="text" >
                                @error('number')
                                <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>

                            <div class="col-md-4">
                               <div class="form-group">
                                   <label for="vid"> video<span class="text-danger"></span></label>
                                    <input  class="form-control" name="vid" type="file" >
                                    @error('image')
                                    <div class="alert alert-danger">{{ $message }}</div>
                                    @enderror
                    </div>





                   </div>
<div style="padding-left:660px ">

                   {{-- <a class="btn btn-outline-primary btn-md  btn-lg" type="submit" href="/Event/event/create" >save and create another</a> --}}

                  <button type="submit" style="margin: 10px;" class="btn btn-primary btn-md  btn-lg" >Save</button>
                </div>
                </form>


            </div>
        </div>
    </div>
</div>
