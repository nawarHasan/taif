<!DOCTYPE html>
<html lang="en">

<head>
      <meta charset="UTF-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <link rel="stylesheet" href="../css/sendReportResearch.css">
      <link rel="preconnect" href="https://fonts.googleapis.com">
      <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
      <link href="https://fonts.googleapis.com/css2?family=Montserrat:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap"
            rel="stylesheet">
      <title>Taif</title>
</head>

<body>

      <!-- NAV BAR -->
      <header>
            <nav>
                  <div class="logo">
                        <h1><i class="fa-brands fa-slack"></i> Taif</h1>
                  </div>

                  <ul class="nav-links">
                         <li class="nav-link"><a href="/home">home</a></li>
                        <li class="nav-link"><a href="/report/create">Reports</a></li>
                        <li class="nav-link"><a href="/research/create">Researchs</a></li>
                        <li class="nav-link"><a href="/books">books</a></li>
                        <li class="nav-link"><a href="/subjects">subjects</a></li>
                        {{-- <li class="nav-link"><a href="/videos">videos</a></li> --}}
                        @if (Route::has('login'))

                        @auth
                            <b><li class="nav-link">{{auth()->user()->name}}</li></b>
                        @else
                            <a href="{{ route('login') }}" ><li class="nav-link"><a href="/login" class="signup-btn">login</a></li>
                        </a>

                            @if (Route::has('register'))
                                <a href="{{ route('register') }}">  <li class="nav-link"><a href="/register" class="login-btn">signup</a></li>
                            </a>
                            @endif
                        @endauth
                    </div>
                @endif
                        {{-- <li class="nav-link"><a href="/register" class="signup-btn">signup</a></li>
                        <li class="nav-link"><a href="/login" class="login-btn">login</a></li> --}}
                  </ul>
                  <div class="hamburger">
                        <span class="bar"></span>
                        <span class="bar"></span>
                        <span class="bar"></span>
                  </div>

                  <div class="space">

                  </div>
            </nav>
      </header>
      <!-- NAV BAR END -->



      <div class="container"><br><br>
            <!-- ASK AN EXPERT -->
            <div class="ask-expert-con">
                  <p>Send Research</p><br>

                <form method="post" enctype="multipart/form-data" action="{{Route('research.store')}}" autocomplete="off">

                    @csrf
                  <select name="subject" class="subject-select">
                        <option selected disabled class="subject-option">Choose a subject</option>
                        @foreach ($colleges as $college)
                        <optgroup label={{$college->name}}>
                            @foreach($subjects as $subject)

                              <option class="subject-option">{{$subject->name}}</option>
                              @endforeach
                              @endforeach

                        </optgroup>
                  </select>




            <input
            type="text"
            name="hent"
            placeholder="ُEnter Your hent.."
            style="padding-right: 3%;padding-bottom: 20vw; width:89.2vw; ">

                  <div class="date__picker">
                        <p for="start_date">Start Date:</p>
                        <input type="date" name="start_date" id="startDate">
                        <p for="end_date">End Date:</p>
                        <input type="date" name="end_date" id="endDate">
                  </div>


                  <input type="file" value="Pick an image" name="question" class="add-file"><br>
                  <button type="submit"  class="ask-btn">Send my research</button>
                  {{-- <input type="button" value="Send my report" class="ask-btn"> --}}
                </form>
            </div>
      </div>

      <br><br><br><br>
      <br><br><br><br>
      <br><br><br><br>
      <br><br>

      <!-- FOOTER -->
      <footer>
            <div class="con">
                  <h1>
                        Taif Website
                  </h1>
                  <p>© 2022 TAIF. ALL RIGHTS RESERVED.</p>

                  <div class="social__links">
                        <ul>
                              <li><a href="#"><i class="fa-brands fa-facebook"></i></a></li>
                              <li><a href="#"><i class="fa-brands fa-twitter"></i></i></a></li>
                              <li><a href="#"><i class="fa-brands fa-youtube"></i></a></li>
                              <li><a href="#"><i class="fa-brands fa-instagram"></i></a></li>
                        </ul>
                  </div>
            </div>
            <ul class="footer-links">
                  <li class="footer-link"><a href="#">home</a></li>
                  <li class="footer-link"><a href="#">write</a></li>
                  <li class="footer-link"><a href="#">subjects</a></li>
                  <li class="footer-link"><a href="#">videos</a></li>
                  <li class="footer-link"><a href="#">signup</a></li>
                  <li class="footer-link"><a href="#">login</a></li>

            </ul>

      </footer>
      </div>
      <link href="https://cdn.quilljs.com/1.3.6/quill.snow.css" rel="stylesheet">
      <script src="https://cdn.quilljs.com/1.3.6/quill.js"></script>
      <script src="https://kit.fontawesome.com/2c1b23ff4c.js" crossorigin="anonymous"></script>
      <script src="../js/nav.js"></script>
      <script src="../js/home.js"></script>
</body>

</html>
