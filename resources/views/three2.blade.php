<meta charset="UTF-8">
<link rel="preconnect" href="https://fonts.googleapis.com">
<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
<link href="https://fonts.googleapis.com/css2?family=Montserrat:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap"
      rel="stylesheet">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link rel="stylesheet" href="../css/books.css">
<link rel="stylesheet" href="../css/lectures.css">

<table id="table_id" class="display">
    <thead>
        <tr>
            {{-- <th>{{trans('admin_trans.Id')}}</th> --}}
            <th>Questions</th>
            <th>Reports</th>
            <th>Researchs</th>
            {{-- @role('user') --}}




            {{-- @endrole --}}
        </tr>

    </thead>


    <tbody>
        <tr>
<td>

    @foreach ($qus as $qu)

    <a href="{{Route('questions.show',$qu->id)}}" class="btn btn-primary btn-sm">
<div class="book-card">

<embed style="height: 250px; width:255px" src="../images/Quesions/{{$qu->question}}">


          <h3>Quest for encluded Growth in Iraq</h3>
          <p>employment explanation of the life</p>
          <p class="author">Mark Blade</p>
    </div>

</div>
</a>
@endforeach
  </div>
</td>

                    <td style="padding-left: 150px">

                        @foreach ($reports as $report)

                        <a href="{{Route('reports.show',$report->id)}}" class="btn btn-primary btn-sm">
                  <div class="book-card">

                    <embed style="height: 250px; width:255px" src="../images/reports/{{$report->question}}">

                              <h3>Quest for encluded Growth in Iraq</h3>
                              <p>employment explanation of the life</p>
                              <p class="author">Mark Blade</p>
                        </div>

                  </div>
            </a>
        @endforeach
    </div>
                    </td>



                    <td style="padding-left: 150px">



                            @foreach ($researchs as $research)

                            <a href="{{Route('books.show',$research->id)}}" class="btn btn-primary btn-sm">
                      <div class="book-card">

                        <embed style="height: 250px; width:255px" src="../images/researchs/{{$research->question}}">


                                  <h3>Quest for encluded Growth in Iraq</h3>
                                  <p>employment explanation of the life</p>
                                  <p class="author">Mark Blade</p>
                            </div>

                      </div>
                </a>
                @endforeach
                          </div>

                    </td>
                </tr>
                </tbody>
