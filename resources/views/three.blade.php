<!DOCTYPE html>
<html lang="en">

<head>
      <meta charset="UTF-8">
      <link rel="preconnect" href="https://fonts.googleapis.com">
      <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
      <link href="https://fonts.googleapis.com/css2?family=Montserrat:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap"
            rel="stylesheet">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <link rel="stylesheet" href="../css/books.css">
      <link rel="stylesheet" href="../css/lectures.css">
      <title>Buy and rent books</title>
</head>

<body>








        </div>

        <!-- HEROSEC END -->

        <!-- SEARCH BAR -->



        <!-- SEARCH BAR END -->

        <!-- FILTER TABS -->
        <br>


        <!-- FILTER TABS END -->


        <!-- BOOKS VIEW -->

        <div class="books-con">
              <br>
              <div class="con-head">
                    <i class="fa-solid fa-book"></i>
                    <h3>Reports</h3>
              </div>

              <div class="books-view">

                @foreach ($reports as $report)

                <a href="{{Route('books.show',$report->id)}}" class="btn btn-primary btn-sm">
          <div class="book-card">

            <embed style="height: 250px; width:255px" src="../images/reports/{{$report->question}}">

                <div class="detailes">
                      <h3>Quest for encluded Growth in Iraq</h3>
                      <p>employment explanation of the life</p>
                      <p class="author">Mark Blade</p>
                </div>

          </div>
    </a>
@endforeach
              </div>

    <div class="books-con">
          <br>
          <div class="con-head">
                <i class="fa-solid fa-book"></i>
                <h3>Researchs</h3>
          </div>

          <div class="books-view">

            @foreach ($researchs as $research)

            <a href="{{Route('books.show',$research->id)}}" class="btn btn-primary btn-sm">
      <div class="book-card">

        <embed style="height: 250px; width:255px" src="../images/researchs/{{$research->question}}">

            <div class="detailes">
                  <h3>Quest for encluded Growth in Iraq</h3>
                  <p>employment explanation of the life</p>
                  <p class="author">Mark Blade</p>
            </div>

      </div>
</a>
@endforeach
          </div>


          <div class="books-con">
            <br>
            <div class="con-head">
                  <i class="fa-solid fa-book"></i>
                  <h3>Quesions</h3>
            </div>

            <div class="books-view">

              @foreach ($qus as $qu)

              <a href="{{Route('books.show',$qu->id)}}" class="btn btn-primary btn-sm">
        <div class="book-card">

          <embed style="height: 250px; width:255px" src="../images/Quesions/{{$qu->question}}">

              <div class="detailes">
                    <h3>Quest for encluded Growth in Iraq</h3>
                    <p>employment explanation of the life</p>
                    <p class="author">Mark Blade</p>
              </div>

        </div>
  </a>
  @endforeach
            </div>
