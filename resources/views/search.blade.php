<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="keywords" content="HTML5 Template">
    <meta name="description" content="Webmin - Bootstrap 4 &amp; Angular 5 Admin Dashboard Template">
    <meta name="author" content="potenzaglobalsolutions.com">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <!-- Title -->
<title>   TTTTEst
</title>

<!-- Favicon -->
<link rel="shortcut icon" href="http://127.0.0.1:8000/assets/images/favicon.ico" type="image/x-icon">

<!-- Font -->
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:200,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900">

<!--- Style css -->
<link href="http://127.0.0.1:8000/assets/css/style.css" rel="stylesheet">


<link href="http://127.0.0.1:8000/assets/datatables/datatables.min.css" rel="stylesheet">

<link href="http://127.0.0.1:8000/assets/datatables/jquery.dataTables.min.css" rel="stylesheet">

<link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.2.1/css/all.min.css" rel="stylesheet">


<!--- Style css -->
{{-- <div class="container" style="margin-top: 50px;">
    <div class="row">
        <div class="col-lg-3"></div>
            <div class="col-lg-6">
                <h3 class="text-center">Laravel Autocomplete Search Box</h3><hr>
                <div class="form-group">
                    <h4>Type by id, name and email!</h4>
                    <input type="text" name="search" id="search" placeholder="Enter search name" class="form-control" onfocus="this.value=''">
                </div>
                <div id="search_list"></div>
            </div>
        <div class="col-lg-3"></div>
    </div>
</div>
<script src="https://cdn.jsdelivr.net/npm/jquery@3.6.0/dist/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
<script>
    $(document).ready(function(){
        $('#search').on('keyup',function(){
            var query= $(this).val();
            $.ajax({
                url:"search",
                type:"GET",
                data:{'search':query},
                success:function(data){
                    $('#search_list').html(data);
                }
            });

        });
    });
</script> --}}
 <form action="{{route('subjects.search')}}" method="get">
<label for="name">search</label>
<input type="text" name="name" placeholder="enter subjectName">
<button type="submit" class="btn btn-info">search</button>

 </form>
<table class="table" class="display">
    <thead>
        <tr>
            <th>Subject</th>
            <th >College</th>
        </tr>
    </thead>
    <tbody>

        @foreach ($subjects as $subject)
        <tr>
            <td>
        <a href="{{Route('subjects.show',$subject->name)}}" style="text-decoration-line: none"> {{ $subject->name }}
        </a>
        </td>
            <td>{{ $subject->colleges->name }}</td>
            @endforeach
        </tr>


    </tbody>
</table>
</div>
</div>
</div>
