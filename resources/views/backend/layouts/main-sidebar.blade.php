<div class="container-fluid">
    <div class="row">
        <!-- Left Sidebar start-->
        <div class="side-menu-fixed">
            <div class="scrollbar side-menu-bg">
                <ul class="nav navbar-nav side-menu" id="sidebarnav">
                    <link href="{{ asset('assets/cssb/dash.css') }}" rel="stylesheet">

                    <li>
                            <span class="right-nav-text">Admin Dashboard</span> </a>
                    </li>

                    <!-- menu title -->
                    <li class="mt-10 mb-10 text-muted pl-4 font-medium menu-title"> </li>


                    <!-- menu admins-->
                    <li>
                        <a href="javascript:void(0);" data-toggle="collapse" data-target="#admins-menu">
                            <div class="pull-left"><i class="fa fa-user" aria-hidden="true"></i><span
                                    class="right-nav-text">Admin</span></div>
                            <div class="pull-right"><i class="ti-plus"></i></div>
                            <div class="clearfix"></div>
                        </a>

                    </li>

                     <!-- menu supervisors-->
                     <li>
                        <a href="javascript:void(0);" data-toggle="collapse" data-target="#supervisors-menu">
                            <div class="pull-left"><i class="fa fa-user" aria-hidden="true"></i><span
                                    class="right-nav-text">Student</span></div>
                            <div class="pull-right"><i class="ti-plus"></i></div>
                            <div class="clearfix"></div>
                        </a>

                    </li>

                      <!-- menu users-->
                      <li>
                        <a href="javascript:void(0);" data-toggle="collapse" data-target="#users-menu">
                            <div class="pull-left"><i class="fa fa-user" aria-hidden="true"></i><span
                                    class="right-nav-text">Admin</span></div>
                            <div class="pull-right"><i class="ti-plus"></i></div>
                            <div class="clearfix"></div>
                        </a>

                    </li>

                    <!-- menu doctors-->
                    <li>
                        <a href="javascript:void(0);" data-toggle="collapse" data-target="#doctors-menu">
                            <div class="pull-left"><i class="fa fa-user-md" aria-hidden="true"></i><span
                                    class="right-nav-text">Admin</span></div>
                            <div class="pull-right"><i class="ti-plus"></i></div>
                            <div class="clearfix"></div>
                        </a>

                    </li>

                      <!-- menu schedules-->
                      <li>
                        <a href="javascript:void(0);" data-toggle="collapse" data-target="#schedules-menu">
                            <div class="pull-left"><i class="fa fa-calendar" aria-hidden="true"></i><span
                                    class="right-nav-text">Admin</span></div>
                            <div class="pull-right"><i class="ti-plus"></i></div>
                            <div class="clearfix"></div>
                        </a>

                    </li>



                    <!-- menu patients-->
                    <li>
                        <a href="javascript:void(0);" data-toggle="collapse" data-target="#patients-menu">
                            <div class="pull-left"><i class="fa fa-user" aria-hidden="true"></i><span
                                    class="right-nav-text">Admin</span></div>
                            <div class="pull-right"><i class="ti-plus"></i></div>
                            <div class="clearfix"></div>
                        </a>

                    </li>





                    <!-- menu appointments-->
                    <li>
                        <a href="javascript:void(0);" data-toggle="collapse" data-target="#appointments-menu">
                            <div class="pull-left"><i class="fa fa-stethoscope"></i><span
                                    class="right-nav-text">Admin</span></div>
                            <div class="pull-right"><i class="ti-plus"></i></div>
                            <div class="clearfix"></div>
                        </a>

                    </li>


                      <!-- menu medical_centers-->
                      <li>
                        <a href="javascript:void(0);" data-toggle="collapse" data-target="#medical_centers-menu">
                            <div class="pull-left"><i class="fa fa-hospital-o" aria-hidden="true"></i><span
                                    class="right-nav-text">Admin</span></div>
                            <div class="pull-right"><i class="ti-plus"></i></div>
                            <div class="clearfix"></div>
                        </a>

                    </li>


                      <!-- menu labs-->
                      <li>
                        <a href="javascript:void(0);" data-toggle="collapse" data-target="#labs-menu">
                            <div class="pull-left"><i class="fa fa-hospital-o" aria-hidden="true"></i><span
                                    class="right-nav-text">Admin</span></div>
                            <div class="pull-right"><i class="ti-plus"></i></div>
                            <div class="clearfix"></div>
                        </a>

                    </li>



                      <!-- menu reports-->
                      <li>
                        <a href="javascript:void(0);" data-toggle="collapse" data-target="#reports-menu">
                            <div class="pull-left"><i class="fa fa-hospital-o" aria-hidden="true"></i><span
                                    class="right-nav-text">Forms</span></div>
                            <div class="pull-right"><i class="ti-plus"></i></div>
                            <div class="clearfix"></div>
                        </a>
                        <ul id="reports-menu" class="collapse" data-parent="#sidebarnav">
                            <li> <a href="">All_Form</a> </li>
                            <li> <a href="">Trash_Form</a> </li>

                        </ul>
                    </li>







                        </ul>
                    </li>
                </ul>
            </div>
        </div>

        <!-- Left Sidebar End-->

        <!--=================================
