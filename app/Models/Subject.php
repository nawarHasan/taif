<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Subject extends Model
{
    use HasFactory;
    protected $table = "subjects";
    protected $fillable=['name','id','doctor_id','college_id'];
   // protected $hidden=;
    // public $timestamps = true;

     public function colleges(){
         return $this ->belongsTo('App\Models\College','college_id','id');
     }
     public function doctor(){
        return $this -> belongsTo('App\Models\Doctor','doctor_id');


    }
    public function student(){
        return $this -> belongsTo('App\Models\Student','student_id');

}
}
