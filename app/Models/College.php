<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class College extends Model
{
    use HasFactory;
    protected $table = "colleges";
    protected $fillable=['name','id'];
    public $timestamps = true;


public function subjects(){
    return $this -> hasMany('App\Models\Subject','college_id','id');
}
}
