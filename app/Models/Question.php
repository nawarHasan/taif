<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Question extends Model
{
    use HasFactory;
    protected $table = "questions";
    protected $fillable=['question','id','student_id','doctor_id','photo','text'];
    public $timestamps = true;
    public function students(){
        return $this ->belongsTo('App\Models\Student','student_id','id');

    }
    public function doctors(){
        return $this ->belongsTo('App\Models\Doctor','doctor_id','id');
    }
}
