<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Doctor extends Authenticatable
{
    // use HasFactory;
    // protected $table = "doctors";
     /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable=['name','id','subject_id', 'profile_picture'];
    public $timestamps = true;
    public function subjectdoc(){
        return $this ->  belongsTo('App\Models\Subject','subject_id');
    }

    public function subjectstu(){
        return $this ->  belongsTo('App\Models\Subject','subject_id');
    }
    // public function questions(){
    //     return $this -> hasMany('App\Models\Question','student_id','id');
    // }
    // public function subjects(){
    //     return $this -> hasMany('App\Models\Subject','college_id','id');
    // }
     protected  $guard  ="doctor";
}
