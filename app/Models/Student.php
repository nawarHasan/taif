<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User;
use Laravel\Cashier\Billable;

class Student extends User
{
    use HasFactory,Billable;
    protected $fillable=['name','id','subject_id', 'profile_picture'];

    public function questions(){
        return $this -> hasMany('App\Models\Question','student_id','id');
    }
    public function student(){
        return $this -> belongsTo('App\Model\Subject','student_id');
    }
}
