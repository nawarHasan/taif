<?php

namespace App\Http\Middleware;

use Illuminate\Auth\Middleware\Authenticate as Middleware;

class Authenticate extends Middleware
{
    /**
     * Get the path the user should be redirected to when they are not authenticated.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return string|null
     */
    protected function redirectTo($request)
    {
//  if($request->has(auth('doctor')->user())){
    //     if (! $request->expectsJson()) {
    //         return route('not');

    // }
    if ($request->is('doctor/*')){
    if (! $request->expectsJson()) {
            return route('save.doctor.login');
        }
    }
    if ($request->is('user/*')){
        if (! $request->expectsJson()) {
                return route('login');
            }
        }

        if ($request->is('student/*')){
            if (! $request->expectsJson()) {
                return route('save.student.login');
                }
            }

            if ($request->is('ask/*')){
                if (! $request->expectsJson()) {
                    return route('save.student.login');
                    }
                }

    }
}
