<?php

namespace App\Http\Traits;

use App\Providers\RouteServiceProvider;

trait AuthTrait
{
    public function chekGuard($request){

        if($request->type == 'student'){
            $guardName= 'student';
        }
        elseif ($request->type == 'admin'){
            $guardName= 'admin';
        }
        elseif ($request->type == 'doctor'){
            $guardName= 'doctor';
        }
        else{
            $guardName= 'web';
        }
        return $guardName;
    }

    public function redirect($request){

        if($request->type == 'student'){
            return redirect()->intended(RouteServiceProvider::STUDENT);
        }
        elseif ($request->type == 'admin'){
            return redirect()->intended(RouteServiceProvider::ADMIN);
        }
        elseif ($request->type == 'doctor'){
            return redirect()->intended(RouteServiceProvider::DOCTOR);
        }
        else{
            return redirect()->intended(RouteServiceProvider::HOME);
        }
    }
}
