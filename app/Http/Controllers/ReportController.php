<?php

namespace App\Http\Controllers;

use App\Models\College;
use App\Models\Report;
use App\Models\Subject;
use Illuminate\Http\Request;
use thiagoalessio\TesseractOCR\TesseractOCR;
use thiagoalessio\TesseractOCR\TesseractOcrException;
class ReportController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // $colleges=College::where('name','Math')->get();
        //  $subjects=Subject::where('college_id','2')->get();
        //  $subjects=Subject::get();
        //  return $subjects[0]->colleges;
       // return view('html.report.create',compact('colleges','subjects'));
// return $college;

        //  $sub = Subject::with(['colleges' => function ($q) {
        //     $q;
        // }])->find(1);
        // return $sub=$sub->colleges;

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // $colleges=College::where('name','Math')->get();
        // $subjects=Subject::where('college_id','2')->get();
        $colleges= College::all();
        $subjects= Subject::all();
        return view('html.report.create',compact('colleges','subjects'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // $request->validate([
        //     'event_name'=>'required',
        //    'event_description'=>'required',
        //    'gregorian_date'=>'required',
        //    'hijri_date'=>'required'
        // ]);

         $report= new Report();
         $report->start_date=$request->start_date;
         $report->end_date=$request->end_date;
         $report->subject=$request->subject;
         $report->hent=$request->hent;
        //  if($request->hasFile('question'))
        //  {
        //    $file_ext=$request->question->getClientOriginalExtension() ;
        //    $file_name=time().'.'.$file_ext;
        //    $path='images/reports';
        //    $request->question->move($path,$file_name);
        //    $report->question=$file_name;
        //  }

        if($request->hasFile('question'))
        {
          $file_ext=$request->question->getClientOriginalExtension() ;
          $file_name=time().'.'.$file_ext;
          $path='images/reports';
          $request->question->move($path,$file_name);
        //   $report->question=$file_name;
        //   return $file_name;
        //   return '<embed style="height: 250px; width:255px" src="../images/reports/{{$file_name}}">';
          echo '<img src="images/reports/'.$file_name.'" style="width:100%;">';


           shell_exec('"C:\\Program Files (x86)\\Tesseract-OCR\\tesseract" "C:\\Users\\llllllllll\\Desktop\\Taif-New\\Taif-Proj\\public\\images\\reports\\'.$file_name.'" out');
           echo "<br><h3>OCR after reading</h3><br><pre>";

      $myfile = fopen("out.txt", "r") or die("Unable to open file!");
     $img2str=fread($myfile,filesize("out.txt"));
     echo $img2str;
     $report->question=$img2str;
     fclose($myfile);
     echo $myfile ;

     echo "</pre>";

    }
          $report->save();
         return redirect()->route('reports.index');
        // return "Nawar";
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function ocr(Request $request)
    {
        $report= new Report();
                if($request->hasFile('question'))
         {
           $file_ext=$request->question->getClientOriginalExtension() ;
           $file_name=time().'.'.$file_ext;
           $path='images/reports';
           $request->question->move($path,$file_name);
           $oc=$report->question=$file_name;
        //    $new =  '_' . time() . '_' . str_replace(array('!', "@", '#', '$', '%', '^', '&', ' ', '*', '(', ')', ':', ';', ',', '?', '/'. '\\', '~', '`', '-'), '_', strtolower($oc));
// return $oc;

         }

    }
    public function ocr2()
    {
        $ocr = new TesseractOCR();
        $ocr->image('oocr.PNG');
        return $ocr->run();
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    public function show($id)
    {
        $report= Report::find($id);
        return view('html.report.show',compact('report'));
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
