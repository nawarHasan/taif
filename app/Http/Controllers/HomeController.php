<?php

namespace App\Http\Controllers;

use App\Models\Book;
use Illuminate\Http\Request;
use App\Models\Doctor;
use App\Models\Question;
use App\Models\Report;
use App\Models\Research;
use App\Models\Subject;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->middleware('auth')->except('index,dashboard');
    }

    public function search(Request $request){

        $qus= Question::when($request->subject,function($q,$val){
            $q->where('subject','LIKE',"%$val%");
        })
        ->get();

        $reports= Report::when($request->subject,function($q,$val){
            $q->where('subject','LIKE',"%$val%");
        })
        ->get();

        $researchs= Research::when($request->subject,function($q,$val){
            $q->where('subject','LIKE',"%$val%");
        })
        ->get();
return view('three',compact('qus','researchs','reports'));

    }

    public function search2(Request $request){

        $qus= Question::when($request->subject,function($q,$val){
            $q->where('text','LIKE',"%$val%");
        })
        ->get();

        $reports= Report::when($request->subject,function($q,$val){
            $q->where('name','LIKE',"%$val%");
        })
        ->get();

        $researchs= Research::when($request->subject,function($q,$val){
            $q->where('name','LIKE',"%$val%");
        })
        ->get();
return view('three2',compact('qus','researchs','reports'));

    }
    public function index()
        {
            return view('auth\selection');
        }


    public function dashboard()
        {
            return view('dashboard');
        }



    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
//     public function index()
//     {
//         return view('html.testask');
//     }
// }
//////////////////Test Role Auth/////////////
public function userHome($type)
{
    $type=['web','doctor','admin'];
    return view('home',["msg"=>"I am user role"],compact('type'));
}
public function doctorHome($type)
{
    $type=['web','doctor','admin','student'];
    return view('home',["msg"=>"I am Doctor role"],compact('type'));
}

public function adminHome()
{
    $type='admin';
    return view('home',["msg"=>"I am Admin role"],compact('type'));
}
}


