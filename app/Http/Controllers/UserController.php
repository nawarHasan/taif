<?php

namespace App\Http\Controllers;
use App\Models\User;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Support\Facades\Auth;
// use Illuminate\Contracts\Auth;
use Illuminate\Support\Facades\Hash;
use Spatie\Permission\Traits\HasRoles;
use Spatie\Permission\Contracts\Role;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Illuminate\Contracts\Auth\Authenticatable as UserContract;
class UserController extends Controller
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth')->except('index');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {

        return view('html.home');
    }
    public function profile()
    {   $user=auth()->user();
        return view('profile.index',compact('user'));
    }

    public function profileEdit($id)
    {
        // $user=auth()->user();
        // return view('profile.edit')->with('user',$user);
        $user = User::findOrFail($id);
        return view('profile.edit',compact('user'));
    }

    public function profileUpdate(Request $request,$id)
    {
        $request->validate([
            'name'  => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users,email,'.auth()->id()
        ]);

        // if ($request->hasFile('profile_picture')) {
        //     $profile = Str::slug(auth()->user()->name).'-'.auth()->id().'.'.$request->profile_picture->getClientOriginalExtension();
        //     $request->profile_picture->move(public_path('images/profile'), $profile);
        // } else {
        //     $profile = 'nawar.png';
        // }
        $user =  User::findOrFail($id);
        $user->name = $request->name;
        $user->email = $request->email;
        if ($request->hasFile('photo')) {
            $file_ext=$request->photo->getClientOriginalExtension() ;
                    $file_name=time().'.'.$file_ext;
                    $path='images/profile';
                    $request->photo->move($path,$file_name);
                    $user->profile_picture=$file_name;
                }


        //$user=auth()->user() tutorial
          // $user =new User();
        // $user=auth()->user();
        //  $user =new User;
        //  $user=DB::table('users')
        //  ->where([
        //     'name'              => auth()->user()->name,
        //     'email'             => auth()->user()->email,
        //     'profile_picture'   => $profile])
        //     ->update([
        //         'name'              => $request->name,
        //         'email'             => $request->email,
        //         'profile_picture'   => $profile]);
        // $user->save();
        $user->save();
        return redirect()->route('profile');
    }

    /**
     * CHANGE PASSWORD
     */
    public function changePasswordForm()
    {
        return view('profile.changepassword');
    }

    public function changePassword(Request $request)
    {
        if (!(Hash::check($request->get('currentpassword'), Auth::user()->password))) {
            return back()->with([
                'msg_currentpassword' => 'Your current password does not matches with the password you provided! Please try again.'
            ]);
        }
        if(strcmp($request->get('currentpassword'), $request->get('newpassword')) == 0){
            return back()->with([
                'msg_currentpassword' => 'New Password cannot be same as your current password! Please choose a different password.'
            ]);
        }

        $this->validate($request, [
            'currentpassword' => 'required',
            'newpassword'     => 'required|string|min:8|confirmed',
        ]);

        // $user=auth()->user();
        // $user=new User;
        $user=DB::table('users')
        ->where(['password'=>auth()->user()->password])

        ->update( ['password' => bcrypt($request->newpassword)]);

        // $user->password = bcrypt($request->get('newpassword'));
        // $user->save();

        Auth::logout();
        return redirect()->route('login');
    }
}
