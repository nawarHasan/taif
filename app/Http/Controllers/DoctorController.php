<?php

namespace App\Http\Controllers;

use App\Models\Answer;
use App\Models\College;
use App\Models\Question;
use App\Models\Report;
use App\Models\Subject;
use Illuminate\Http\Request;

class DoctorController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth:doctor');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(){
        $colleges= College::all();
        $subjects= Subject::all();
        return view('html.doctor.index',compact('subjects','colleges'));
    }


public function select(){
    return view('html.doctor.select');
}



    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function answerStore(Request $request)
    {
        $answer=new Answer();
        if ($request->hasFile('question')) {
            $file_ext=$request->question->getClientOriginalExtension() ;
                    $file_name=time().'.'.$file_ext;
                    $path='images/Answer';
                    $request->question->move($path,$file_name);
                    $answer->answer=$file_name;
                }
                $answer->save();
                return redirect()->route('choose.question');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

//////Start Question //////
public function questionChoose(){
    $questions=Question::all();
    return view('html.doctor.question.qchoose',compact('questions'));
}
public function askQ($id){
    $question=Question::findOrFail($id);
    return view('html.doctor.question.ask',compact('question'));
}

//// End Question///////



    ///// Start Report/////
    public function reportChoose(){
        $questions=Report::all();
        return view('html.doctor.report.Rechoose',compact('questions'));
    }

    public function askRe($id){
        $question=Report::findOrFail($id);
        return view('html.doctor.report.ask',compact('question'));
    }



    ////// End Report
}
