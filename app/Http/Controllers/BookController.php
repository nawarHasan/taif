<?php

namespace App\Http\Controllers;

use App\Models\Book;
use Illuminate\Http\Request;

class BookController extends Controller
{
    // public function __construct()
    // {
    //      $this->middleware('auth');
    // }

    public function index(){
        $books= Book::all();
        return view('html.book.index',compact('books'));
    }


    public function create()
    {
        return view('html.book.create');
}

    public function store(Request $request)
    {
        $book= new Book;
        $book->name=  $request->name;
        $book->author=$request->author;
        $book->number=$request->number;
        $book->user_id=$request->user_id;
        if ($request->hasFile('photo')) {
            $file_ext=$request->photo->getClientOriginalExtension() ;
                    $file_name=time().'.'.$file_ext;
                    $path='images/books';
                    $request->photo->move($path,$file_name);
                    $book->image=$file_name;
                }
                $book->save();
                return redirect()->route('books.index');
    }

    public function show($id)
    {
               $book= Book::find($id);
               return view('html.book.show',compact('book'));
    }


    public function edit($id)
    {
        $book= Book::find($id);
        return view('html.book.edit',compact('book'));

    }



     public function update(Request $request, $id)
    {
        $book= Book::findOrFail($id);
        $book->name=  $request->name;
        $book->author=$request->author;
        $book->number=$request->number;
        $book->user_id=$request->user_id;
        if ($request->hasFile('photo')) {
            $file_ext=$request->photo->getClientOriginalExtension() ;
                    $file_name=time().'.'.$file_ext;
                    $path='images/books';
                    $request->photo->move($path,$file_name);
                    $book->image=$file_name;
                }
                $book->save();
                return redirect()->route('books.index');
    }


     public function destroy($id)
    {
        //
    }
}
