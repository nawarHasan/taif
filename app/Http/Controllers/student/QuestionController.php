<?php

namespace App\Http\Controllers\student;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Symfony\Component\HttpKernel\HttpCache\Store;
use App\Models\College;
use App\Models\Question;
use App\Models\Subject;
class QuestionController extends Controller
{
    public function create()
    {
        // $colleges=College::all();
        // return view('html.testask')->with('colleges',$colleges);
        $num=[1,2,3];

    $colleges1=College::select('name')->where('id',1)->get();
    $subjects1=Subject::select('name')->where('college_id','1')->get();
         $subjects1 = Subject::whereHas('colleges',function($q) {
            $q->where('college_id','1');
         })->get();
         /////////////////////////////////////////////////////// 2
         $colleges2=College::select('name')->where('id',2)->get();
    $subjects2=Subject::select('name')->where('college_id','2')->get();
         $subjects2 = Subject::whereHas('colleges',function($q) {
            $q->where('college_id','2');
         })->get();
/////////////////////////////////////////////////////// 3
         $colleges3=College::select('name')->where('id',3)->get();
         $subjects3=Subject::select('name')->where('college_id','3')->get();
              $subjects3 = Subject::whereHas('colleges',function($q) {
                 $q->where('college_id','3');
              })->get();
         return view('html.question.create',compact('colleges1','subjects1','colleges2','subjects2','colleges3','subjects3'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $question = new Question();
        $question->subject=$request->subject;
        $question->hent=$request->hent;
         // طريقة لانشاء objs
        //  $questions=new Question;

        //   $file_ext= $request->question->getClientOriginalExtension();
        //   $file_name=time().'.'.$file_ext;
        //   $path='images/Ques';
        //   $request->question->move($path,$file_name);
        //  $questions->question=$file_name;
        //   $qs = Question::create([
        //      'question' => $file_name,
        //   ]);
     //    $questions->question=$request->input('question');
        //  $x=$questions->question=$request->photo->store('image');;
        // $questions->student_id=auth()->user()->id;

        if($request->hasFile('image'))
        {
          $file_ext=$request->image->getClientOriginalExtension() ;
          $file_name=time().'.'.$file_ext;
          $path='images/Quesions';
          $request->image->move($path,$file_name);
          $question->question=$file_name;
        shell_exec('"C:\\Program Files (x86)\\Tesseract-OCR\\tesseract" "C:\\Users\\llllllllll\\Desktop\\Taif-New\\Taif-Proj\\public\\images\\Quesions\\'.$file_name.'" out');
        echo "<br><h3>OCR after reading</h3><br><pre>";

   $myfile = fopen("out.txt", "r") or die("Unable to open file!");
  $img2str=fread($myfile,filesize("out.txt"));
  echo $img2str;
  $question->text=$img2str;
  fclose($myfile);
  echo $myfile ;

  echo "</pre>";

 }
   $question->save();

   return redirect()->route('questions.create');

        // return view('html.testimage',compact('x'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $question= Question::find($id);
        return view('html.question.show',compact('question'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    public function pdf(){
        return view('html\pdf');
    }
}


