<?php

namespace App\Http\Controllers;

use  App\Http\Controllers\ZainCash\JWT;
// require (__DIR__.'/../../autoload.php');
// require_once __DIR__ . '/ZainCash' . '/autoload.php';
// use  App\Http\Controllers\ZainCash\autoload;
// use App\Http\Controllers\ZainCash\cre
// require('\App\Http\Controllers\credentials.php');
use  App\Http\Controllers\ZainCash\credentials;
use Exception;
// use  App\Http\Controllers\ZainCash;
// use  App\Http\Controllers\ZainCash;
// use  App\Http\Controllers\ZainCash;

use Illuminate\Http\Request;

class ZainCashController extends Controller
{
    public function checkout(){
        $config=config('services.zaincash');
        $data = [
            'amount'  =>  $config['amount'],
            'serviceType'  => $config['service_type'],
            'msisdn'  => $config['msisdn'],
            'orderId'  => $config['order_id'],
            'redirectUrl'  => $config['redirection_url'],
            'iat'  => time(),
            'exp'  => time()+60*60*4
            ];
            $newtoken = JWT::encode(
                $data,      //Data to be encoded in the JWT
                $config['secret'],'HS256'
                );
                $tUrl = 'https://test.zaincash.iq/transaction/init';
                $rUrl = 'https://test.zaincash.iq/transaction/pay?id=';
                if($config['production_cred']){
                    $tUrl = 'https://api.zaincash.iq/transaction/init';
                    $rUrl = 'https://api.zaincash.iq/transaction/pay?id=';
                }else{
                    $tUrl = 'https://test.zaincash.iq/transaction/init';
                    $rUrl = 'https://test.zaincash.iq/transaction/pay?id=';
                }
                $data_to_post = array();
                $data_to_post['token'] = urlencode($newtoken);
                $data_to_post['merchantId'] =  $config['merchantid'];
                $data_to_post['lang'] =        $config['language'];
                $options = array(
                'http' => array(
                'header'  => "Content-type: application/x-www-form-urlencoded\r\n",
                'method'  => 'POST',
                'content' => http_build_query($data_to_post),
                ),
                );


                $context  = stream_context_create($options);

                $response= file_get_contents($tUrl, false, $context);
                //Parsing response
                try{
                    $hostname = gethostbyaddr($_SERVER['REMOTE_ADDR']);
                } catch(Exception $e){
                    $hostname = 'unknown';
                }
                $array = json_decode($response, true);
                //  dd(  $data_to_post['token'] );
                $transaction_id = $array['id'];
                $newurl=$rUrl.$transaction_id;
               header('Location: '.$newurl);
               return  redirect()->away($newurl);

    }
    public function zainCancel(){
        $config=config('services.zaincash');
        if (isset($_GET['token'])){

            //you can decode the token by this PHP code:
            $result= JWT::decode($_GET['token'], $config['secret'], array('HS256'));
            echo json_encode($result);
            $result= (array) $result;

            //And to check for status of the transaction, use $result['status'], like this:
            if ($result['status']=='success'){
                //Successful transaction

                //$result will be like this example:
                /*
                array(5) {
                    ["status"]=>
                    string(7) "success"
                    ["orderid"]=>
                    string(9) "Bill12345"
                    ["id"]=>
                    string(24) "58650f0f90c6362288da08cf"
                    ["iat"]=>
                    int(1483018052)
                    ["exp"]=>
                    int(1483032452)
                }
                */
            }
            if ($result['status']=='failed'){
                //Failed transaction and its reason
                $reason=$result['msg'];



                //$result will be like this example:
                /*
                array(6) {
                    ["status"]=>
                    string(6) "failed"
                    ["msg"]=>
                    string(33) "Invalid credentials for requester"
                    ["orderid"]=>
                    string(9) "Bill12345"
                    ["id"]=>
                    string(24) "58650ca990c6362288da08c8"
                    ["iat"]=>
                    int(1483017397)
                    ["exp"]=>
                    int(1483020997)
                }
                */

            }
        } else {
            //Cancelled transaction (if he clicked "Cancel and go back"

            //NO TOKEN HERE, SO NO $result
        }
    }

}
