<?php

namespace App\Http\Controllers\Auth;
// use App\Http\Traits\AuthTrait;
use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

     use AuthenticatesUsers;
    // use AuthTrait;
        protected $guard='web';
    public function __construct(Request $request)
    {
        // $this->middleware('guest')->except('logout');
       if ($request->is('admin/*'))
       $this->guard='admin';

       if ($request->is('student/*'))
       $this->guard='student';

       if ($request->is('doctor/*'))
       $this->guard='doctor';

    }

    public function loginForm($guard){
        if($guard=='admin')
        return view('html.auth.loginAdmin');

        elseif($guard=='doctor')
            return view('html.auth.loginDoctor');

            elseif($guard=='student'){
                return view('html.auth.loginStudent');}


                    else{
                        return view('auth.login');}
            }


    public function login(Request $request)
    {
        $input = $request->all();
        $this->validate($request,[
            'email'=>'required|email',
            'password'=>'required'
        ]);

        if(Auth::guard($this->guard)->attempt(['email'=>$input["email"], 'password'=>$input['password']]))
        {
             return redirect()->route('home');
            }

        else
        {
            return redirect()
            ->route("login")
            ->with("error",'Incorrect email or password');
        }
    }


    //////////////////
    public function loginAdmin(Request $request)
    {
        $input = $request->all();
        $this->validate($request,[
            'email'=>'required|email',
            'password'=>'required'
        ]);

        if(Auth::guard($this->guard)->attempt(['email'=>$input["email"], 'password'=>$input['password']]))
        {
             return redirect()->route('home');
            }

        else
        {
            return redirect('admin/login')
            ->with("error",'Incorrect email or password');
        }
    }

////////////

public function loginDoctor(Request $request)
{
    $input = $request->all();
    $this->validate($request,[
        'email'=>'required|email',
        'password'=>'required'
    ]);

    if(Auth::guard($this->guard)->attempt(['email'=>$input["email"], 'password'=>$input['password']]))
    {
         return redirect()->route('home');
        }

    else
    {
        return redirect('doctor/login')
        ->with("error",'Incorrect email or password');
    }
}

/////////////////
public function loginStudent(Request $request)
{
    $input = $request->all();
    $this->validate($request,[
        'email'=>'required|email',
        'password'=>'required'
    ]);

    if(Auth::guard($this->guard)->attempt(['email'=>$input["email"], 'password'=>$input['password']]))
    {
         return redirect()->route('home');
        }

    else
    {
        return redirect('student/login')
        ->with("error",'Incorrect email or password');
    }
}
//////////////
    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    // protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * //@return void
     */
    // public function __construct()
    // {
    //     $this->middleware('guest')->except('logout');
    // }

    // public function loginForm($type){

    //     return view('auth.login',compact('type'));
    // }

    // public function login(Request $request){

    //     if (Auth::guard($this->chekGuard($request))->attempt(['email' => $request->email, 'password' => $request->password])) {
    //        return $this->redirect($request);
    //     }
    //     else return redirect('nawar');

    // }

     public function logout(Request $request)
     {
         Auth::guard($this->guard)->logout();

         $request->session()->invalidate();

        $request->session()->regenerateToken();

         return redirect('/');
     }


    }
