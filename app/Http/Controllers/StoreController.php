<?php

namespace App\Http\Controllers;

use App\Models\Book;
use App\Models\Store;
use Illuminate\Http\Request;

class StoreController extends Controller
{
    public function index(){
        $products= Store::all();
        return view('html.store.index',compact('products'));
    }
    public function create()
    {
        return view('html.store.create');
}
public function store(Request $request)
{
    $store= new Store();
    $store->name=  $request->name;
    $store->price=$request->price;

    if ($request->hasFile('photo')) {
        $file_ext=$request->photo->getClientOriginalExtension() ;
                $file_name=time().'.'.$file_ext;
                $path='images/products';
                $request->photo->move($path,$file_name);
                $store->image=$file_name;
            }
            $store->save();
            return redirect()->route('products.index');
}
}
