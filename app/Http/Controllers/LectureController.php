<?php

namespace App\Http\Controllers;

use App\Models\College;
use App\Models\Lecture;
use Illuminate\Http\Request;
use App\Models\Subject;

class LectureController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    // public function __construct()
    // {
    //     $this->middleware('auth');
    // }

    public function index()
    {
    //     $colecture= College::all();
    //     $sublecture= Subject::all();
    //     return view('html\lecture\index',[
    //     'colecture'=>$colecture,
    //     'sublecture'=>$sublecture
    //     ]);
    //      $lectures= Lectures::all();
    //     return view('html.subject.index',compact('subjects','colleges'));
    // }
    //  }
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $lectures=Lecture::all();
        return view('html.lecture.create',compact('lectures'));
    }

    public function download(Request $request ){
     return response()->download(public_path('/images/Quesions/' . '1678225489.pdf'));
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $lecture= new Lecture();
        $lecture->name=  $request->name;
        if ($request->hasFile('photo')) {
            $file_ext=$request->photo->getClientOriginalExtension() ;
                    $file_name=time().'.'.$file_ext;
                    $path='images/lectures';
                    $request->photo->move($path,$file_name);
                    $lecture->file=$file_name;
                }
                $lecture->save();
                return redirect()->route('subjects.index');
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function detail($id)
    {
        $data=Subject::find($id);
        return view('html.lecture.detail',
         ['lectures'=>$data]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
