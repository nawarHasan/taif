<?php

namespace App\Http\Controllers;

use App\Models\College;
use App\Models\Lecture;
use App\Models\Subject;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class SubjectController extends Controller
{

    public function search(Request $request){

        $subjects= Subject::when($request->name,function($q,$val){
            $q->where('name','LIKE',"%$val%");
        })
        ->get();
return view('html.subject.index',compact('subjects'));

    }
    public function index(){
        $colleges= College::all();
        $subjects= Subject::all();
        // $request=request();
        // $name=;
        return view('html.subject.index',compact('subjects','colleges'));
    }


    public function create()
    {
        $colleges=College::all();
        return view('html.subject.create',compact('colleges'));
}

    public function store(Request $request)
    {
        $subject= new Subject();
        $subject->name=  $request->name;
        $subject->college_id=$request->college_id;
        if ($request->hasFile('file')) {
            $file_ext=$request->photo->getClientOriginalExtension() ;
                    $file_name=time().'.'.$file_ext;
                    $path='images/subjects';
                    $request->photo->move($path,$file_name);
                    $subject->file=$file_name;
                }
                $subject->save();
                return redirect()->route('subjects.index');
    }

    public function show($name)
    {

              $lecture= Lecture::where('name','=',$name)->get();
           // $lecture= DB::table('lectures')->where('name',$name)->get();
           return view('html.subject.show',['lecture'=> $lecture[0]
]);

    }

    public function edit($id)
    {
        $subject= Subject::find($id);
        return view('html.subject.edit',compact('subject'));

    }



     public function update(Request $request, $id)
    {
        $subject= Subject::findOrFail($id);
        $subject->name=  $request->name;
        $subject->author=$request->author;
        $subject->number=$request->number;
        $subject->user_id=$request->user_id;
        if ($request->hasFile('photo')) {
            $file_ext=$request->photo->getClientOriginalExtension() ;
                    $file_name=time().'.'.$file_ext;
                    $path='images/subjects';
                    $request->photo->move($path,$file_name);
                    $subject->image=$file_name;
                }
                $subject->save();
                return redirect()->route('subjects.index');
    }


     public function destroy($id)
    {
        //
    }
}


