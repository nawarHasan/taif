<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\College;
use App\Models\Research;
use App\Models\Subject;
class ResearchController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $colleges= College::all();
        $subjects= Subject::all();
        return view('html.research.create',compact('colleges','subjects'));

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $research= new Research();
        $research->start_date=$request->start_date;
        $research->end_date=$request->end_date;
        $research->subject=$request->subject;
        $research->hent=$request->hent;
        if($request->hasFile('question'))
        {
          $file_ext=$request->question->getClientOriginalExtension() ;
          $file_name=time().'.'.$file_ext;
          $path='images/researchs';
          $request->question->move($path,$file_name);
        //   $report->question=$file_name;
        //   return $file_name;
        //   return '<embed style="height: 250px; width:255px" src="../images/reports/{{$file_name}}">';


           shell_exec('"C:\\Program Files (x86)\\Tesseract-OCR\\tesseract" "C:\\Users\\llllllllll\\Desktop\\Taif-New\\Taif-Proj\\public\\images\\researchs\\'.$file_name.'" out');
           echo "<br><h3>OCR after reading</h3><br><pre>";

      $myfile = fopen("out.txt", "r") or die("Unable to open file!");
     $img2str=fread($myfile,filesize("out.txt"));
     echo $img2str;
     $research->question=$img2str;
     fclose($myfile);
     echo $myfile ;

     echo "</pre>";

    }
         $research->save();
        return redirect()->route('research.index');
       // return "Nawar";
   }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
