<?php

namespace App\Notifications;

use App\Models\Question;
use App\Models\Student;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class NewNotification extends Notification
{
    use Queueable;
    protected $student;
    protected $question;
    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(Question $question,Student $student)
    {
        $this->$question=$question;
        $this->$student=$student;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
      $via=['database'];
      if($notifiable->notify_mail){
        $via[]='mail';
      }
      return $via ;
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toDatabase($notifiable)
    {
        $body=sprintf(
            '%s ask question %s',
            $this->student->name,
            $this->question->text,

        );
        return [
            'title'=>'New Notification',
            'body'=>'new Question',
            'url'=>'', //????
        ];
    }

    public function toMail($notifiable)
    {
        return (new MailMessage)
                    ->line('The introduction to the notification.')
                    ->action('Notification Action', url('/'))
                    ->line('Thank you for using our application!');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
